/**
  *********************************************************************************************************
  *
  *	This file is part of <<PROJECT NAME>>.
  *	
  *	  <<PROJECT NAME>> is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  <<PROJECT NAME>> is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with <<PROJECT NAME>>.  If not, see <http://www.gnu.org/licenses/>.
  *	
  * @file file.c
  * @brief < Кратко о назначениии модуля >
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/**----- Инклуды -----------------------------------------------------------------------------------------*/
#include <stdint.h>              // Нормальные типы данных
#include <stdbool.h>             // тип boolean
#include <math.h>                // библиотека математических функций

/** @addtogroup FILE
  * @{
  */
 /** @addtogroup Macros
  * @{
  */
/**----- Макросы константы -------------------------------------------------------------------------------*/



/**----- Макрофункции ------------------------------------------------------------------------------------*/
#define CAT(ARGS)                   __CAT(ARGS)
#define __CAT(x, y)                   x ## y

/** @} */
/** @defgroup ExportedFunctions
  * @{
  */
/**----- Типы данных -------------------------------------------------------------------------------------*/



/** @} */
/** @defgroup ExportedFunctions
  * @{
  */
/**----- Глобальные прототипы функций --------------------------------------------------------------------*/
void FILE_Init( void );



/** @} */
 /** @addtogroup Macros
  * @{
  */
/**----- Проверка макросов -------------------------------------------------------------------------------*/



/** @} */
/**----- КОНЕЦ ФАЙЛА -------------------------------------------------------------------------------------*/
/** @} */

#ifdef __cplusplus
} // extern "C" block end
#endif
