/*
* This is an independent project of an individual developer. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
/**********************************************************************************************************
  *
  *	This file is part of <<PROJECT NAME>>.
  *	
  *	  <<PROJECT NAME>> is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  <<PROJECT NAME>> is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with <<PROJECT NAME>>.  If not, see <http://www.gnu.org/licenses/>.
  *	
  * @file file.c
  * @brief < Кратко о назначениии модуля >
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */
/**----- Инклуды -----------------------------------------------------------------------------------------*/
#include "file.h"

/** 
 * @defgroup FILE
 * @{
 */
/** @defgroup Macros
  * @{
  */
/**----- Макросы константы -------------------------------------------------------------------------------*/



/**----- Макросы -----------------------------------------------------------------------------------------*/



/** @} */ /* End of group Macros */
/** @defgroup Privatetypes
  * @{
  */
/**----- Локальные типы данных ---------------------------------------------------------------------------*/



/** @} */ /* End of group Private types */
/** @defgroup PrivateVariable
  * @{
  */
/**----- Локальные переменные ----------------------------------------------------------------------------*/



/** @} */ /* End of group Private variable */
/** @defgroup PublicVariable
  * @{
  */
/**----- Глобальные переменные ---------------------------------------------------------------------------*/



/** @} */ /* End of group Public variable */
/** @defgroup ExternVariable
  * @{
  */
/**----- Внешние переменные ------------------------------------------------------------------------------*/



/** @} */ /* End of group Extern variable */
/** @defgroup PrivateFunctionPrototypes
  * @{
  */
/**----- Прототипы локальных функций ---------------------------------------------------------------------*/



/** @} */ /* End of group Private Function Prototypes */
/** @defgroup ExternFunctionPrototypes
  * @{
  */
/**----- Прототипы внешних функций -----------------------------------------------------------------------*/



/** @} */ /* End of group Extern Function Prototypes */
/** @defgroup RTOSFunctionPrototypes
  * @{
  */
/**----- Прототипы функций RTOS --------------------------------------------------------------------------*/



/** @} */ /* End of group RTOS Function Prototypes */
/** @defgroup PublicFunction 
  * @{
  */
/**----- Глобальные функции ------------------------------------------------------------------------------*/
/** 
@function FILE_Init
Инициализирует модуль FILE
*/
void FILE_Init( void )
{
    
}

/** @} */ /* End of group Public Function */
/** @defgroup PrivateFunction 
  * @{
  */
/**----- Локальные функции -------------------------------------------------------------------------------*/



/** @} */ /* End of group Private Function */
/** @defgroup IRQFunction 
  * @{
  */
/**----- Функции обработки прерываний --------------------------------------------------------------------*/



/** @} */ /* End of group IRQ */
/** @defgroup RTOSFunction 
  * @{
  */
/**----- Функции RTOS ------------------------------------------------------------------------------------*/



/** @} */ /* End of group RTOS Function */
/**----- КОНЕЦ ФАЙЛА -------------------------------------------------------------------------------------*/
/** @} */ /* End of module */
