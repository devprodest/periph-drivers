/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *	
  * @file usr_math_conf.h
  * @brief Библиотека математических функций
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */
#ifndef USR_MATH_CONF_H
#define USR_MATH_CONF_H

//----- Инклуды --------------------------------------------------------------------------------
#include <stdint.h>
//----- Макросы --------------------------------------------------------------------------------

//#define MATH_OLD_NAME_SUPPORT             ///< Будут ли использоваться старые имена функций
#define MATH_ABS_ASM                      ///< Будет использоваться ассемблерная реализация функции MATH_abs 

/** Необходимо закоментировать те функции которые не нужны, что бы не занимать место в памяти */
#define USE_MATH_SQRT                     ///< Включает в сборку функцию MATH_sqrt
#define USE_MATH_ABS                      ///< Включает в сборку функцию MATH_abs
#define USE_MATH_POW                      ///< Включает в сборку функцию MATH_GetMPPTSunVoltage
#define USE_MATH_CRC_16_CCITT             ///< Включает в сборку функцию MATH_pow
#define USE_MATH_CRC_16_USERTABLE         ///< Включает в сборку функцию MATH_CRC_16_ccitt
#define USE_MATH_BUTTERWORTH5             ///< Включает в сборку функцию MATH_CRC_16_UserTable
#define USE_MATH_HEXSTRTOINT              ///< Включает в сборку функцию MATH_Butterworth5
#define USE_MATH_DECSTRTOINT              ///< Включает в сборку функцию MATH_HEXStrToInt
#define USE_MATH_GETMPPTSUNVOLTAGE        ///< Включает в сборку функцию MATH_DECStrToInt
#define MATH_ITOA                         ///< Включает в сборку функцию MATH_itoa
#define USE_MATH_RoL                      ///< Включает в сборку функцию MATH_RoL





#ifdef MATH_ITOA
    #define USE_MATH_ABS
#endif
//----- КОНЕЦ ФАЙЛА ----------------------------------------------------------------------------
#endif
