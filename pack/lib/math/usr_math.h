/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *	
  * @file usr_math.h
  * @brief Библиотека математических функций
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */
#ifndef USR_MATH_H
#define USR_MATH_H

#ifdef __cplusplus
extern "C" {
#endif

//----- Инклуды --------------------------------------------------------------------------------
#include <stdint.h>
#include <usr_math_conf.h>
#ifdef MATH_ITOA
    #include <stdbool.h>
    #include <string.h>
#endif
//----- Макросы --------------------------------------------------------------------------------

/* для обратной совместимести со страрыми проектами */
#ifdef MATH_OLD_NAME_SUPPORT
    #define usr_sqrt                MATH_sqrt
    #define usr_abs                 MATH_abs
    #define getCRC_16_CCITT         MATH_CRC_16_ccitt
#endif




/** Функция MATH_hypot(X, Y) − Быстрое вычисление гипотенузы. X и Y положительные целые числа */
#define MATH_hypot(X, Y)	( ((X) > (Y)) ? (X) + ( (Y) >> 1 ) : (Y) + ( (X) >> 1 )

/** Функция MATH_max(X, Y) − Максимальное значение. */
#define MATH_max(X, Y)		( ((X) > (Y)) ? (X) : (Y) )

/** Функция MATH_min(X, Y) − Минимальное значение. */
#define MATH_min(X, Y)		( ((X) < (Y)) ? (X) : (Y) )

/** Функция MATH_exp2(X) − Вычисление 2^X */
#define MATH_exp2(X)		(1 << (X))

//----- Прототипы функций ----------------------------------------------------------------------

#ifdef USE_MATH_RoL
    /**
    @function MATH_RoL() - Циклически сдвигает биты влево в обрпеделенной базе
    @param uint32_t value − значение которое необходимо сдвигуть
    @param uint8_t offset − на сколько необходимо сдвинуть
    @param uint8_t base − размер действительных битов value
    @return uint32_t результат сдвига
    */
    uint32_t MATH_RoL( uint32_t value, uint8_t offset, uint8_t base);
#endif

#ifdef USE_MATH_SQRT
    /**
    @function MATH_sqrt(x) − Вычисление квадратного корня.
    @param uint32_t value − число квадратный корень которого нужно вычислять
    @retval uint32_t значение квадратного корня
    */
    uint32_t MATH_sqrt(register uint32_t value);
    /**
    @function MATH_sqrt(x) − Вычисление квадратного корня.
    @param uint32_t value − число квадратный корень которого нужно вычислять
    @retval uint32_t значение квадратного корня
    */
    uint8_t MATH_sqrt8(register uint8_t value);
    /**
    @function MATH_sqrt16(x) − Вычисление квадратного корня.
    @param uint16_t value − число квадратный корень которого нужно вычислять
    @return uint16_t значение квадратного корня
     */
    uint16_t MATH_sqrt16(register uint16_t value);
#endif

#ifdef USE_MATH_ABS
    /**
    @function MATH_abs(x) − Вычисление модуля числа.
    @param uint32_t value − число модуль которого необходимо узнать
    @retval int32_t значение модуля
    */
    uint32_t MATH_abs(int32_t value);
#endif

#ifdef USE_MATH_GETMPPTSUNVOLTAGE
    /**
    @function MATH_GetMPPTSunVoltage(u, i, du, di) − Вычисление оптимального напряжения MPPT
    @param uint32_t u_sun − Текущее напряжение солнечного модуля
    @param uint32_t i_sun − Текущий ток солнечного модуля
    @param uint32_t du_sun − Абсолютное приращение напряжения солнечного модуля
    @param uint32_t di_sun − Абсолютное приращение тока солнечного модуля
    @retval int32_t значение оптимального напряжения
    */
    uint32_t MATH_GetMPPTSunVoltage(uint32_t u_sun, uint32_t i_sun, uint32_t du_sun, uint32_t di_sun);
#endif

#ifdef USE_MATH_POW
    /**
    @function MATH_pow - Вычисление степени числа
    @param uint16_t x − число
    @param uint16_t x − степень
    @retval int32_t результат 
    */
    int32_t MATH_pow(int16_t x, uint8_t y);
#endif

#ifdef USE_MATH_CRC_16_CCITT
    /**
    @function MATH_CRC_16_ccitt() − Вычисление контрольной суммы.
    @param uint8_t *buf − адрес буфера который считаем
    @param uint16_t len − длина буфера
    @retval uint16_t значение контрольной суммы
    */
    uint16_t MATH_CRC_16_ccitt(uint8_t *buf, uint16_t len);
#endif

#ifdef USE_MATH_CRC_16_USERTABLE
    /**
    @function MATH_CRC_16_UserTable() − Вычисление CRC16 с пользовательскими настройками
    @param uint8_t *buf − адрес буфера который считаем
    @param uint16_t len − длина буфера
    @param uint16_t *CRCtable − указатель на таблицу CRC16
    @param uint16_t initCRC − начальное значение
    @retval uint16_t значение контрольной суммы
    */
    uint16_t MATH_CRC_16_UserTable(const uint8_t *buf, uint16_t len, const uint16_t *CRCtable, uint16_t initCRC);
#endif

#ifdef USE_MATH_BUTTERWORTH5
    /**
    @function MATH_Butterworth5 - фильтр Баттерворта 5-го порядка
    @param uint16_t input − очередное входное значение
    @param uint16_t *buf − указатель на буфер размерностью [5]
    @retval uint16_t результат фильтрации
    */
    uint16_t MATH_Butterworth5(uint16_t input, uint16_t *buf );
#endif


#ifdef USE_MATH_INT16TOHEXSTR
/**
@function MATH_Int16ToHexStr преобразует число в HEX строку
@param[in] uint16_t in - число
@param[out] char * out - Указатель на буфер куда выполнится преобразование

@example

    char string[4];
    MATH_Int16ToHexStr(0xFEBC, string);  // string = "FACD"

*/
void MATH_Int16ToHexStr (uint16_t in, char * out);
#endif


#ifdef USE_MATH_HEXSTRTOINT
    /**
    @function MATH_HEXStrToInt преобразует строку в число
    @param char *str - строка в которой записано число в HEX формате без "0x", 0..FFFFFFFF
    @param  uint8_t len - длина строки в символах
    @retval uint32_t результат преобразования

    @example

        char *string = "FACD";
        uint32_t integer = MATH_HEXStrToInt(string, 4);  // integer = 0xFACD

    */
    uint32_t MATH_HEXStrToInt(const char *str, uint8_t len);
#endif

#ifdef USE_MATH_DECSTRTOINT
    /**
    @function MATH_DECStrToInt преобразует строку в число
    @param char *str - строка в которой записано число в десятичном формате 0..4 294 967 295
    @param  uint8_t len - длина строки в символах
    @retval uint32_t результат преобразования

    @example

        char *string = "1234";
        uint32_t integer = MATH_DECStrToInt(string, 4);  // integer = 1234

    */
    uint32_t MATH_DECStrToInt(const char *str, uint8_t len);
#endif

#ifdef MATH_ITOA
    /**
    @function MATH_itoa преобразует строку в число
    @param int32_t n - число
    @param char *str - буфер для сохранения строки
    @param uint8_t len - размер буфера
    @retval bool - true если успешно
    @example

        uint32_t n = 1234;
        char* string[5];
        bool result = MATH_itoa(n, string, 5);  // string = "1234"

    */
    bool MATH_itoa(int32_t n, char *str, uint8_t len);
#endif

#ifdef __cplusplus
}
#endif
//----- КОНЕЦ ФАЙЛА ----------------------------------------------------------------------------
#endif
