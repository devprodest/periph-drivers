/*
* This is an independent project of an individual developer. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
/* LINTLIBRARY */
/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *	
  * @file usr_math.c
  * @brief Библиотека математических функций. Файл реализации
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */
//----- Инклуды ---------------------------------------------------------------------------------
#include "usr_math.h"

//----- Переменные ------------------------------------------------------------------------------

//----- Функции ---------------------------------------------------------------------------------

/** 
 * @defgroup userMath Различного рода математические функции
 * @{
 */
#ifdef USE_MATH_RoL
/**
@function MATH_RoL() - Циклически сдвигает биты влево в опеделенной базе
@param uint32_t value − значение которое необходимо сдвигуть
@param uint8_t offset − на сколько необходимо сдвинуть
@param uint8_t base − размер действительных битов value
@return uint32_t результат сдвига
 */
uint32_t MATH_RoL( uint32_t value, uint8_t offset, uint8_t base)
{
    return value << offset | value >> (base - offset);
}
#endif

#ifdef USE_MATH_SQRT

#if (__ARMCC_VERSION >= 6010050)
#else
#pragma push
#pragma Otime
#endif
/**
@function MATH_sqrt(x) − Вычисление квадратного корня.
@param uint32_t value − число квадратный корень которого нужно вычислять
@return uint32_t значение квадратного корня
 */
uint32_t MATH_sqrt(register uint32_t value)
{
    /*lint -save -e40 -e10 -e845 -e522 */
    register uint32_t d = 0, ax = 0;
    
    // Находим количество битов дополняющих до 32, по сути a + b = 32, где и номер старшего бита
    // Затем из 32 вычитаем значение, получаем номер старшего бита b = 32 - a и кладем в 'а'
    
    #if (__ARMCC_VERSION >= 6010050)
        __asm volatile (
        "CLZ    %[A], %[VALUE]"           "\n\t"
        "RSB    %[A], %[A], 0x1F"
                : [A]"=&r" (ax)
                : [VALUE]"r" (value), [A]"r" (ax)
        );
    #else
        __asm {
            CLZ    ax, value
            RSB    ax, ax, 0x1F
        };
    #endif
    
    /** Нахождение квадратного корня методом Ньютона. Основная формула для вычисления:
    *     sqrt = (sqrt' + value / sqrt') / 2
    *  где:
    *     value - значение корень которого находим
    *     sqrt - текущее значение приближения
    *     sqrt' - значение приближения из предыдущего шага
    *  Для ускорения первым приближением выбирается значение равное value сдвинутому на половину
    *  номера старшего значащего единичного бита, т.е. для чила 0001101001011011 = 1101001
    */
    d = value >> (ax >> 1);    // первое приближение
    ax = (d + value / d) >> 1;
    d = (ax + value / ax) >> 1;
    ax = (d + value / d) >> 1;
    value = (ax + value / ax) >> 1;
    /*lint -restore */
    return value;
}

/**
@function MATH_sqrt8(x) − Вычисление квадратного корня.
@param uint8_t value − число квадратный корень которого нужно вычислять
@return uint8_t значение квадратного корня
 */
uint8_t MATH_sqrt8(register uint8_t value)
{
    /*lint -save -e40 -e10 -e845 -e522 */
    register uint8_t d = 0, ax = 0;
    
    // Находим количество битов дополняющих до 32, по сути a + b = 32, где и номер старшего бита
    // Затем из 32 вычитаем значение, получаем номер старшего бита b = 32 - a и кладем в 'а'
    
    #if (__ARMCC_VERSION >= 6010050)
        __asm volatile (
        "CLZ    %[A], %[VALUE]"           "\n\t"
        "RSB    %[A], %[A], 0x1F"
                : [A]"=&r" (ax)
                : [VALUE]"r" (value), [A]"r" (ax)
        );
    #else
        __asm {
            CLZ    ax, value
            RSB    ax, ax, 0x1F
        };
    #endif
    
    /** Нахождение квадратного корня методом Ньютона. Основная формула для вычисления:
    *     sqrt = (sqrt' + value / sqrt') / 2
    *  где:
    *     value - значение корень которого находим
    *     sqrt - текущее значение приближения
    *     sqrt' - значение приближения из предыдущего шага
    *  Для ускорения первым приближением выбирается значение равное value сдвинутому на половину
    *  номера старшего значащего единичного бита, т.е. для чила 0001101001011011 = 1101001
    */
    d = value >> (ax >> 1);    // первое приближение
    ax = (d + value / d) >> 1;
    value = (ax + value / ax) >> 1;
    /*lint -restore */
    return value;
}

/**
@function MATH_sqrt16(x) − Вычисление квадратного корня.
@param uint16_t value − число квадратный корень которого нужно вычислять
@return uint16_t значение квадратного корня
 */
uint16_t MATH_sqrt16(register uint16_t value)
{
    /*lint -save -e40 -e10 -e845 -e522 */
    register uint16_t d = 0, ax = 0;
    
    // Находим количество битов дополняющих до 32, по сути a + b = 32, где и номер старшего бита
    // Затем из 32 вычитаем значение, получаем номер старшего бита b = 32 - a и кладем в 'а'
    
    #if (__ARMCC_VERSION >= 6010050)
        __asm volatile (
        "CLZ    %[A], %[VALUE]"           "\n\t"
        "RSB    %[A], %[A], 0x1F"
                : [A]"=&r" (ax)
                : [VALUE]"r" (value), [A]"r" (ax)
        );
    #else
        __asm {
            CLZ    ax, value
            RSB    ax, ax, 0x1F
        };
    #endif
    
    /** Нахождение квадратного корня методом Ньютона. Основная формула для вычисления:
    *     sqrt = (sqrt' + value / sqrt') / 2
    *  где:
    *     value - значение корень которого находим
    *     sqrt - текущее значение приближения
    *     sqrt' - значение приближения из предыдущего шага
    *  Для ускорения первым приближением выбирается значение равное value сдвинутому на половину
    *  номера старшего значащего единичного бита, т.е. для чила 0001101001011011 = 1101001
    */
    d = value >> (ax >> 1);    // первое приближение
    ax = (d + value / d) >> 1;
    d = (ax + value / ax) >> 1;
    value = (d + value / d) >> 1;
    /*lint -restore */
    return value;
}


#if (__ARMCC_VERSION >= 6010050)
#else
#pragma pop
#endif

#endif

#ifdef USE_MATH_ABS
/**
@function MATH_abs(x) − Вычисление модуля числа.
@param uint32_t value − число модуль которого необходимо узнать
@return int32_t значение модуля
 */
uint32_t MATH_abs(int32_t value)
{
#ifdef MATH_ABS_ASM
     /*lint -save -e40 -e10 -e845 -e522 */
    #if (__ARMCC_VERSION >= 6010050)
        __asm volatile (
        "CMP    %[A], 0x00"             "\n\t"
		"IT		MI"                     "\n\t"
        "RSBMI    %[A], %[A], 0x00"
                : [A]"=&r" (value)
                : [A]"r" (value)
        );
    #else
        __asm {
            CMP      value, 0x00
			IT       MI
			RSBMI    value,value, 0x00
        };
    #endif 
    /*lint -restore */
	return value;
#else
    return (uint32_t)(( value > 0 )? value: (0 - value));
#endif
}
#endif

#ifdef USE_MATH_GETMPPTSUNVOLTAGE
/**
@function MATH_GetMPPTSunVoltage(u, i, du, di) − Вычисление оптимального напряжения MPPT
@param uint32_t u_sun − Текущее напряжение солнечного модуля
@param uint32_t i_sun − Текущий ток солнечного модуля
@param uint32_t du_sun − Абсолютное приращение напряжения солнечного модуля
@param uint32_t di_sun − Абсолютное приращение тока солнечного модуля
@return int32_t значение оптимального напряжения
 */
uint32_t MATH_GetMPPTSunVoltage(uint32_t u_sun, uint32_t i_sun, uint32_t du_sun, uint32_t di_sun)
{
    return MATH_sqrt( (u_sun * du_sun / di_sun) * i_sun );
}
#endif

#ifdef USE_MATH_POW
/**
@function MATH_pow - Вычисление степени числа
@param uint16_t x − число
@param uint16_t x − степень
@return int32_t результат 
*/
int32_t MATH_pow(int16_t x, uint8_t y)
{
    int32_t result = 1;
    
    while( y-- ) result *= x;
    return result;
}
#endif

#ifdef USE_MATH_CRC_16_CCITT
/**
@function MATH_CRC_16_ccitt() − Вычисление контрольной суммы.
@param uint8_t *buf − адрес буфера который считаем
@param uint16_t len − длина буфера
@return uint16_t значение контрольной суммы
 */
uint16_t MATH_CRC_16_ccitt(uint8_t *buf, uint16_t len)
{
    uint16_t crc = 0xFFFF;
	uint16_t crctab;
	uint8_t byte;
	
    while (len--)
    {
        crctab = 0x0000;
        /// Магия
        byte = ( *buf++ )^( crc >> 8 );
        if( byte & 0x01 ) crctab ^= 0x1021;
        if( byte & 0x02 ) crctab ^= 0x2042;
        if( byte & 0x04 ) crctab ^= 0x4084;
        if( byte & 0x08 ) crctab ^= 0x8108;
        if( byte & 0x10 ) crctab ^= 0x1231;
        if( byte & 0x20 ) crctab ^= 0x2462;
        if( byte & 0x40 ) crctab ^= 0x48C4;
        if( byte & 0x80 ) crctab ^= 0x9188;
        
        crc = ( ( (crc & 0xFF)^(crctab >> 8) ) << 8 ) | ( crctab & 0xFF );
    }
    return crc;
}
#endif

#ifdef USE_MATH_CRC_16_USERTABLE
/**
@function MATH_CRC_16_UserTable() − Вычисление CRC16 с пользовательскими настройками
@param uint8_t *buf − адрес буфера который считаем
@param uint16_t len − длина буфера
@param uint16_t *CRCtable − указатель на таблицу CRC16
@param uint16_t initCRC − начальное значение
@return uint16_t значение контрольной суммы
 */
uint16_t MATH_CRC_16_UserTable(const uint8_t *buf, uint16_t len, const uint16_t *CRCtable, uint16_t initCRC)
{
    uint16_t i;
    uint16_t crc = initCRC;

    for(i = 0; i < len; i++)
    {
        crc = (crc << 8) ^ CRCtable[(crc >> 8) ^ buf[i]];
    }
    return crc;
}
#endif

#ifdef USE_MATH_BUTTERWORTH5
/**
@function MATH_Butterworth5 - фильтр Баттерворта 5-го порядка
@param uint16_t input − очередное входное значение
@param uint16_t *buf − указатель на буфер размерностью [5]
@return uint16_t результат фильтрации
*/
uint16_t MATH_Butterworth5(uint16_t input, uint16_t *buf )
{
    uint16_t r6, r7, r9;
    
    r7 = *(buf+4) - *buf;
    *buf = input;
    r6 = (r7 >> 1) - (r7 >> 6) - *(buf+4);
    *(buf+4) = *(buf+3);
    *(buf+3) = r6;
    r6 -= r7;
    r7 = *(buf+2) - input;
    r9 = (r7 >> 3) - (r7 >> 6) + *(buf+2);
    r7 -= r9;
    *(buf+2) = *(buf+1);
    *(buf+1) = r7;
    return (r6 - r9) >> 1;
}
#endif


#ifdef USE_MATH_INT16TOHEXSTR
/**
@function MATH_Int16ToHexStr преобразует число в HEX строку
@param[in] uint16_t in - число
@param[out] char * out - Указатель на буфер куда выполнится преобразование

@example

    char string[4];
    MATH_Int16ToHexStr(0xFEBC, string);  // string = "FACD"

*/
void MATH_Int16ToHexStr (uint16_t in, char * out)
{
    for (uint8_t i = 0; i < 4; i++ )
    {
        uint8_t c = (in >> (i * 4)) & 0xF;
        out[3 - i] = (( c > 0 ) ? ('A' - 10) : '0') + c;
    }
}
#endif




#ifdef USE_MATH_HEXSTRTOINT
/**
@function MATH_HEXStrToInt преобразует строку в число
@param char *str - строка в которой записано число в HEX формате без "0x", 0..FFFFFFFF
@param  uint8_t len - длина строки в символах
@return uint32_t результат преобразования

@example

    char *string = "FACD";
    uint32_t integer = MATH_HEXStrToInt(string, 4);  // integer = 0xFACD

*/
uint32_t MATH_HEXStrToInt(const char *str, uint8_t len)
{
    uint32_t value = 0;
    uint8_t i;
    
    for (i = 0; i < len ; i++) value = value << 4 | str[i] - ((str[i] > '9') ? ('A' - 10): '0');
    return value;
}
#endif

#ifdef USE_MATH_DECSTRTOINT
/**
@function MATH_DECStrToInt преобразует строку в число
@param char *str - строка в которой записано число в десятичном формате 0..4 294 967 295
@param  uint8_t len - длина строки в символах
@return uint32_t результат преобразования

@example

    char *string = "1234";
    uint32_t integer = MATH_DECStrToInt(string, 4);  // integer = 1234

*/
uint32_t MATH_DECStrToInt(const char *str, uint8_t len)
{
    uint32_t value = 0;
    uint8_t i;
    
    for (i = 0; i < len ; i++) value = value * 10 + str[i] - '0';
    return value;
}
#endif

#ifdef MATH_ITOA
/**
@function MATH_itoa преобразует строку в число
@param[in] int32_t n - число
@param[out] char *str - буфер для сохранения строки
@param[in] uint8_t len - размер буфера
@return bool - true если успешно
@example

    uint32_t n = 1234;
    char* string[5];
    bool result = MATH_itoa(n, string, 5);  // string = "1234"

*/
bool MATH_itoa(int32_t n, char *str, uint8_t len)
{
    uint8_t i = 0;
    uint8_t j = 0;
    bool sign = (n < 0);
    uint8_t c;
    uint32_t number =  MATH_abs(n); 
    
    do {
        if (i > len) return false;
        str[ i++ ] = number % 10 + '0'; 
    } 
    while ((number /= 10) > 0);

    if (sign) str[i++] = '-';
    str[i] = '\0';
    j = strlen(str) -1;

    for (i = 0; i < j; i++, j--) 
    {
        c = str[ i ];
        str[ i ] = str[ j ];
        str[ j ] = c;
    }
    return true;
}
#endif

/** @} */

//----- КОНЕЦ ФАЙЛА -----------------------------------------------------------------------------
