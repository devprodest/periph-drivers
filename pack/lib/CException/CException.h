/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *
  * @file CException.h
  * @brief CException - облегченная библиотека исключений для C
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * @authors Greg Williams, Doctor Surly, Matt Fletcher, Jordan S, Job Vranish, Mike Karlesky
  * @authors Mark VanderVoord, Scott Vokes
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */
  
#ifndef CEXCEPTION_H
#define CEXCEPTION_H


#ifdef __cplusplus
extern "C" {
#endif

/**----- Инклуды -----------------------------------------------------------------------------------------*/
#include <setjmp.h>
#include <stdint.h>

/**----- Макросы константы -------------------------------------------------------------------------------*/

#ifdef CEXCEPTION_USE_CONFIG_FILE
    #include "CException_config.h"
#endif

//This is the value to assign when there isn't an exception
#ifndef CEXCEPTION_NONE
    #define CEXCEPTION_NONE      (0x5A5A5A5A)
#endif

//This is number of exception stacks to keep track of (one per task)
#ifndef CEXCEPTION_NUM_ID
    #define CEXCEPTION_NUM_ID    (1) //there is only the one stack by default
#endif

//This is the method of getting the current exception stack index (0 if only one stack)
#ifndef CEXCEPTION_GET_ID
    #define CEXCEPTION_GET_ID    (0) //use the first index always because there is only one anyway
#endif

//The type to use to store the exception values.
#ifndef CEXCEPTION_T
    #define CEXCEPTION_T         unsigned int
#endif

//This is an optional special handler for when there is no global Catch
#ifndef CEXCEPTION_NO_CATCH_HANDLER
    #define CEXCEPTION_NO_CATCH_HANDLER(id)
#endif

//These hooks allow you to inject custom code into places, particularly useful for saving and restoring additional state
#ifndef CEXCEPTION_HOOK_START_TRY
    #define CEXCEPTION_HOOK_START_TRY
#endif
#ifndef CEXCEPTION_HOOK_HAPPY_TRY
    #define CEXCEPTION_HOOK_HAPPY_TRY
#endif
#ifndef CEXCEPTION_HOOK_AFTER_TRY
    #define CEXCEPTION_HOOK_AFTER_TRY
#endif
#ifndef CEXCEPTION_HOOK_START_CATCH
    #define CEXCEPTION_HOOK_START_CATCH
#endif

/**----- Типы данных -------------------------------------------------------------------------------------*/

typedef struct {
    jmp_buf*                pFrame;
    CEXCEPTION_T volatile   Exception;
} CEXCEPTION_FRAME_T;

//actual root frame storage (only one if single-tasking)
extern volatile CEXCEPTION_FRAME_T CExceptionFrames[];

/**----- Макрофункции ------------------------------------------------------------------------------------*/

/**
@function Try − начинает защищенный блок
 */
#define Try                                                         \
    {                                                               \
        jmp_buf *PrevFrame, NewFrame;                               \
        unsigned int MY_ID = CEXCEPTION_GET_ID;                     \
        PrevFrame = CExceptionFrames[MY_ID].pFrame;                 \
        CExceptionFrames[MY_ID].pFrame = (jmp_buf*)(&NewFrame);     \
        CExceptionFrames[MY_ID].Exception = CEXCEPTION_NONE;        \
        CEXCEPTION_HOOK_START_TRY;                                  \
        if (setjmp(NewFrame) == 0) {                                \
            if (1)

/**
@function Catch − блок обработки ошибок
 */
#define Catch(e)                                                    \
            else { }                                                \
            CExceptionFrames[MY_ID].Exception = CEXCEPTION_NONE;    \
            CEXCEPTION_HOOK_HAPPY_TRY;                              \
        }                                                           \
        else                                                        \
        {                                                           \
            e = CExceptionFrames[MY_ID].Exception;                  \
            (void)e;                                                \
            CEXCEPTION_HOOK_START_CATCH;                            \
        }                                                           \
        CExceptionFrames[MY_ID].pFrame = PrevFrame;                 \
        CEXCEPTION_HOOK_AFTER_TRY;                                  \
    }                                                               \
    if (CExceptionFrames[CEXCEPTION_GET_ID].Exception != CEXCEPTION_NONE)



/**
@function Throw − используется для немедленного выхода из текущего блока Try
 */
#define ExitTry() Throw(CEXCEPTION_NONE)

/**----- Глобальные прототипы функций --------------------------------------------------------------------*/

void Throw(CEXCEPTION_T ExceptionID);


#ifdef __cplusplus
}
#endif
/**----- КОНЕЦ ФАЙЛА -------------------------------------------------------------------------------------*/
#endif	/* End */
