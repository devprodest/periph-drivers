/*
* This is an independent project of an individual developer. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
/* LINTLIBRARY */
/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *	
  * @file CException.c
  * @brief CException - облегченная библиотека исключений для C
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * @authors Greg Williams, Doctor Surly, Matt Fletcher, Jordan S, Job Vranish, Mike Karlesky
  * @authors Mark VanderVoord, Scott Vokes
  * 
  * @copyright GNU General Public License
  * 
  * @example
  *
  *    // Защищенный блок
  *    Try
  *    {
  *      ...
  *      потенциально_опасный код
  *      if (возникла_ошибка)
  *        Throw(e1); // e1 - код конкретной ошибки
  *      ...  
  *      вызов_потенциально_опасной_функции()
  *      ...
  *    }
  *    // Сюда мы попадаем только в случае возникновения ошибки
  *    Catch(e) // здесь e - код возникшей ошибки
  *    {
  *      ...
  *      код_обработки_ошибки
  *      ...
  *    }
  *    // Конец защищенного блока
  *    ...
  *    
  *    потенциально_опасная_функция()
  *    {
  *      ...
  *      if (возникла_ошибка)
  *        Throw(e2); // e2 - код конкретной ошибки
  *      ...
  *    }
  * 
  *********************************************************************************************************
  */
/**----- Инклуды -----------------------------------------------------------------------------------------*/
#include "CException.h"

/**----- Макросы константы -------------------------------------------------------------------------------*/
/**----- Макросы -----------------------------------------------------------------------------------------*/
/**----- Локальные типы данных ---------------------------------------------------------------------------*/
/**----- Локальные переменные ----------------------------------------------------------------------------*/
volatile CEXCEPTION_FRAME_T CExceptionFrames[CEXCEPTION_NUM_ID] = {{ 0 }};

/**----- Глобальные переменные ---------------------------------------------------------------------------*/
/**----- Прототипы функций -------------------------------------------------------------------------------*/
/**----- Глобальные функции ------------------------------------------------------------------------------*/

/**
@function Throw − Используется для того, чтобы выбросить исключение.
@param CEXCEPTION_T ExceptionID − идентификатор исключения, который будет передан Catch в качестве причины ошибки
 */
void Throw(CEXCEPTION_T ExceptionID)
{
    unsigned int MY_ID = CEXCEPTION_GET_ID;
    CExceptionFrames[MY_ID].Exception = ExceptionID;
    if (CExceptionFrames[MY_ID].pFrame)
    {
        longjmp(*CExceptionFrames[MY_ID].pFrame, 1);
    }
    CEXCEPTION_NO_CATCH_HANDLER(ExceptionID);
}


//------------------------------------------------------------------------------------------
//  Explanation of what it's all for:
//------------------------------------------------------------------------------------------
/*
#define Try
    {                                                                   <- give us some local scope.  most compilers are happy with this
        jmp_buf *PrevFrame, NewFrame;                                   <- prev frame points to the last try block's frame.  new frame gets created on stack for this Try block
        unsigned int MY_ID = CEXCEPTION_GET_ID;                         <- look up this task's id for use in frame array.  always 0 if single-tasking
        PrevFrame = CExceptionFrames[CEXCEPTION_GET_ID].pFrame;         <- set pointer to point at old frame (which array is currently pointing at)
        CExceptionFrames[MY_ID].pFrame = &NewFrame;                     <- set array to point at my new frame instead, now
        CExceptionFrames[MY_ID].Exception = CEXCEPTION_NONE;            <- initialize my exception id to be NONE
        if (setjmp(NewFrame) == 0) {                                    <- do setjmp.  it returns 1 if longjump called, otherwise 0
            if (&PrevFrame)                                             <- this is here to force proper scoping.  it requires braces or a single line to be but after Try, otherwise won't compile.  This is always true at this point.

#define Catch(e)
            else { }                                                    <- this also forces proper scoping.  Without this they could stick their own 'else' in and it would get ugly
            CExceptionFrames[MY_ID].Exception = CEXCEPTION_NONE;        <- no errors happened, so just set the exception id to NONE (in case it was corrupted)
        }
        else                                                            <- an exception occurred
        { e = CExceptionFrames[MY_ID].Exception; e=e;}                  <- assign the caught exception id to the variable passed in.
        CExceptionFrames[MY_ID].pFrame = PrevFrame;                     <- make the pointer in the array point at the previous frame again, as if NewFrame never existed.
    }                                                                   <- finish off that local scope we created to have our own variables
    if (CExceptionFrames[CEXCEPTION_GET_ID].Exception != CEXCEPTION_NONE)  <- start the actual 'catch' processing if we have an exception id saved away
 */

/**----- Локальные функции -------------------------------------------------------------------------------*/

/**----- КОНЕЦ ФАЙЛА -------------------------------------------------------------------------------------*/
