/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *	
  * @file lcd1602.h
  * @brief Заголовочный файл
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */
#ifndef		LCD1602_H
#define		LCD1602_H


#ifdef __cplusplus
extern "C" {
#endif

//----- Подключаемые файлы -------------------------------------------------------------------------------
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "main.h"
#ifdef STM32F2XX
    #include "stm32f2xx.h"
#endif
#ifdef STM32F1XX
    #include "stm32f10x.h"
#endif


//--------------------------------------------------------------------------------------------------------
/*< ИСПОЛЬЗОВАНИЕ ФУНКЦИЙ FREERTOS
*   Если определен, то задержка в милисекундах будет считаться силами операционной системы, иначе циклом 
* вызова функции микросекундных зедаржек
*/

#define LCD_4_BITS                  true
#define LCD_8_BITS                  false
#define LCD_1602                    false
#define LCD_2004                    true




#define LCD1602_USE_FREERTOS        1               /**< Определяет будет ли использоваться FREERTOS */

#define LCD1602_USE_BITS           LCD_8_BITS
#define LCD_TYPE                   LCD_2004





#ifdef LCD1602_USE_FREERTOS

    #include "FreeRTOS.h"
    #include "task.h"
    
#endif

#define MCU_CLOCK_USECOND   (1 << 4)        /**< Количество тиков счетчика микроконтроллера в микросекунду */

//----- Настройки ----------------------------------------------------------------------------------------


#define PORT_RE             GPIOB           /**< Порт сигнала RE, тип GPIO_TypeDef */
#define PIN_RE              GPIO_Pin_5      /**< Пин сигнала RE, GPIO_Pin_x где x равен 0..15 */

#define PORT_RS             GPIOB           /**< Порт сигнала RS, тип GPIO_TypeDef */
#define PIN_RS              GPIO_Pin_4      /**< Пин сигнала RS, GPIO_Pin_x где x равен 0..15 */

#define PORT_DATA0          GPIOC           /**< Порт сигналов DATA, тип GPIO_TypeDef */
#define DATA_PIN0           GPIO_Pin_0      /**< Вывод сигнала DATA4, GPIO_Pin_x где x равен 0..15 */
#define PORT_DATA1          GPIOC           /**< Порт сигналов DATA, тип GPIO_TypeDef */
#define DATA_PIN1           GPIO_Pin_1      /**< Вывод сигнала DATA5, GPIO_Pin_x где x равен 0..15 */
#define PORT_DATA2          GPIOC           /**< Порт сигналов DATA, тип GPIO_TypeDef */
#define DATA_PIN2           GPIO_Pin_2      /**< Вывод сигнала DATA6, GPIO_Pin_x где x равен 0..15 */
#define PORT_DATA3          GPIOC           /**< Порт сигналов DATA, тип GPIO_TypeDef */
#define DATA_PIN3           GPIO_Pin_3      /**< Вывод сигнала DATA7, GPIO_Pin_x где x равен 0..15 */
#define PORT_DATA4          GPIOB           /**< Порт сигналов DATA, тип GPIO_TypeDef */
#define DATA_PIN4           GPIO_Pin_6      /**< Вывод сигнала DATA4, GPIO_Pin_x где x равен 0..15 */
#define PORT_DATA5          GPIOB           /**< Порт сигналов DATA, тип GPIO_TypeDef */
#define DATA_PIN5           GPIO_Pin_7      /**< Вывод сигнала DATA5, GPIO_Pin_x где x равен 0..15 */
#define PORT_DATA6          GPIOB           /**< Порт сигналов DATA, тип GPIO_TypeDef */
#define DATA_PIN6           GPIO_Pin_8      /**< Вывод сигнала DATA6, GPIO_Pin_x где x равен 0..15 */
#define PORT_DATA7          GPIOB           /**< Порт сигналов DATA, тип GPIO_TypeDef */
#define DATA_PIN7           GPIO_Pin_9      /**< Вывод сигнала DATA7, GPIO_Pin_x где x равен 0..15 */

//----- Типы данных --------------------------------------------------------------------------------------
/// Режимы отображения курсора
typedef enum
{
    CURSOR_HIDE     = 0x0,                  ///< Курсор скрыт
    CURSOR_STATIC   = 0x2,                  ///< Курсор в виде подчеркивания
    CURSOR_BLINK    = 0x1,                  ///< Курсор в виде мигающей ячейки (прямоугольника)
    CURSOR_BLINK_   = 0x3                   ///< Курсор в виде мигающей полоски
} CursoreMode;

/// Режимы отображения курсора
typedef enum
{
    LCD_LINE_1   = 0,                       ///< Строка номер 1
    LCD_LINE_2   = 20,                      ///< Строка номер 2
    LCD_LINE_3   = 40,                      ///< Строка номер 3
    LCD_LINE_4   = 60                       ///< Строка номер 4
} LCD2004_LINE;

//----- Приватные функции --------------------------------------------------------------------------------

/*! Функция usec_delay( uint32_t count ) − Задержка в микросекундах
* \param uint32_t count − время задержки, не должно быть больше UINT32_MAX / MCU_CLOCK_USECOND
*/
__inline static void usec_delay( uint32_t count )
{    
    count *= MCU_CLOCK_USECOND;
    while( count-- ){ };
}


#ifdef LCD1602_USE_FREERTOS  

#define msec_delay   vTaskDelay

#else

/*! Функция msec_delay( uint32_t count ) − Задержка в милисекундах
* \param uint32_t count − время задержки, не должно быть больше UINT32_MAX
*/
__inline static void msec_delay( uint32_t count )
{
    assert_param(count >= UINT32_MAX);
    assert_param( count == 0 );
    
    while( count-- ) usec_delay(1000);
}

#endif // LCD1602_USE_FREERTOS



//----- Экспортируемые функции ---------------------------------------------------------------------------
void LCD1602_Init( void );
void LCD1602_WriteUserSymbol( const uint8_t number, const uint8_t *data );

void LCD1602_CursorToHome( void );
void LCD1602_CursorToPosition( const uint8_t position );
void LCD1602_CursorToPositionXY( const uint8_t X, const uint8_t Y );
void LCD1602_CursorMode( const CursoreMode cursor );

void LCD1602_ClearScreen( void );
void LCD1602_ScreenOnOff( const FunctionalState NewState );

void LCD1602_PrintCharOfPosition( const uint8_t position, const char symbol );
void LCD1602_PrintStringOfPosition( const uint8_t position, const char *str );
void LCD1602_PrintNumberOfPosition( const uint8_t position, const int32_t number );

void LCD1602_PrintCharOfPositionXY( const uint8_t X, const uint8_t Y, const char symbol );
void LCD1602_PrintStringOfPositionXY( const uint8_t X, const uint8_t Y, const char *str );
void LCD1602_PrintNumberOfPositionXY( const uint8_t X, const uint8_t Y, const int32_t number );

void LCD1602_AppendChar( const char symbol );
void LCD1602_AppendString( const char *str );
void LCD1602_AppendNumber( const int32_t number );


//----- Обработчики ошибок -----------------------------------------------------------------------------

#if (MCU_CLOCK_USECOND == 0)
    #errore "MCU_CLOCK_USECOND must be equal 1..UINT32_MAX"
#endif


#ifdef __cplusplus
}
#endif
//----- КОНЕЦ ФАЙЛА --------------------------------------------------------------------------------------
#endif
