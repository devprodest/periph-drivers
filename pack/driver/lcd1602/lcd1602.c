/*
* This is an independent project of an individual developer. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *	
  * @file lcd1602.c
  * @brief Реализация функций для работы с LCD1602 и 2004 дисплеями
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */

/* LINTLIBRARY */
#include "lcd1602.h"

// -----------------------------------------------------------------------------------         
static volatile CursoreMode lcd_cursor;


// -----------------------------------------------------------------------------------
static void LCD1602_SendCMD( const uint8_t data );
static void LCD1602_SendData( const uint8_t data );


// -----------------------------------------------------------------------------------

/*! Функция void LCD1602_SendData( uint8_t data ) − Отправляет данные в дисплей
* \param uint8_t data − данные
*/
static void LCD1602_SendData( const uint8_t data )
{
    uint8_t datah = data >> 4 & 0x0F;
    uint8_t datal = data & 0x0F;
    
    PORT_RE->BRR = PIN_RE;
    PORT_RS->BSRR = PIN_RS;
    usec_delay( 1 );

    #if (LCD1602_USE_BITS == LCD_4_BITS)
        
        // старший полубайт
        PORT_DATA4->BRR = DATA_PIN4;
        PORT_DATA5->BRR = DATA_PIN5;
        PORT_DATA6->BRR = DATA_PIN6;
        PORT_DATA7->BRR = DATA_PIN7;
        if (datah & 0x1) { PORT_DATA4->BSRR = DATA_PIN4; };
        if (datah & 0x2) { PORT_DATA5->BSRR = DATA_PIN5; };
        if (datah & 0x4) { PORT_DATA6->BSRR = DATA_PIN6; };
        if (datah & 0x8) { PORT_DATA7->BSRR = DATA_PIN7; };
        usec_delay( 5 );
        
        PORT_RE->BSRR = PIN_RE;
        usec_delay( 5 );
        PORT_RE->BRR = PIN_RE;
        usec_delay( 2 );
        
        // младший полубайт
        PORT_DATA4->BRR = DATA_PIN4;
        PORT_DATA5->BRR = DATA_PIN5;
        PORT_DATA6->BRR = DATA_PIN6;
        PORT_DATA7->BRR = DATA_PIN7;
        if (datal & 0x1) { PORT_DATA4->BSRR = DATA_PIN4; };
        if (datal & 0x2) { PORT_DATA5->BSRR = DATA_PIN5; };
        if (datal & 0x4) { PORT_DATA6->BSRR = DATA_PIN6; };
        if (datal & 0x8) { PORT_DATA7->BSRR = DATA_PIN7; };
        
    #else
        
        PORT_DATA0->BRR = DATA_PIN0;
        PORT_DATA1->BRR = DATA_PIN1;
        PORT_DATA2->BRR = DATA_PIN2;
        PORT_DATA3->BRR = DATA_PIN3;
        PORT_DATA4->BRR = DATA_PIN4;
        PORT_DATA5->BRR = DATA_PIN5;
        PORT_DATA6->BRR = DATA_PIN6;
        PORT_DATA7->BRR = DATA_PIN7;
        
        if (datal & 0x1) { PORT_DATA0->BSRR = DATA_PIN0; };
        if (datal & 0x2) { PORT_DATA1->BSRR = DATA_PIN1; };
        if (datal & 0x4) { PORT_DATA2->BSRR = DATA_PIN2; };
        if (datal & 0x8) { PORT_DATA3->BSRR = DATA_PIN3; };
        if (datah & 0x1) { PORT_DATA4->BSRR = DATA_PIN4; };
        if (datah & 0x2) { PORT_DATA5->BSRR = DATA_PIN5; };
        if (datah & 0x4) { PORT_DATA6->BSRR = DATA_PIN6; };
        if (datah & 0x8) { PORT_DATA7->BSRR = DATA_PIN7; };
        
    #endif
        
    usec_delay( 5 );
    
    PORT_RE->BSRR = PIN_RE;
    usec_delay( 5 );
    PORT_RE->BRR = PIN_RE;
    usec_delay( 1 );
    PORT_RS->BRR = PIN_RS;
    
    usec_delay(200);
}


/*! Функция void LCD1602_SendCMD( uint8_t cmd ) − Отправляет команду в дисплей
* \param uint8_t cmd − код команды
*/
static void LCD1602_SendCMD( const uint8_t cmd )
{
    uint8_t cmdh = cmd >> 4 & 0x0F;
    uint8_t cmdl = cmd & 0x0F;
    
    PORT_RE->BRR = PIN_RE;
    PORT_RS->BRR = PIN_RS;
    usec_delay( 1 );
    
    
    #if (LCD1602_USE_BITS == LCD_4_BITS)
        
        // старший полубайт
        PORT_DATA4->BRR = DATA_PIN4;
        PORT_DATA5->BRR = DATA_PIN5;
        PORT_DATA6->BRR = DATA_PIN6;
        PORT_DATA7->BRR = DATA_PIN7;
        if (cmdh & 0x1) { PORT_DATA4->BSRR = DATA_PIN4; };
        if (cmdh & 0x2) { PORT_DATA5->BSRR = DATA_PIN5; };
        if (cmdh & 0x4) { PORT_DATA6->BSRR = DATA_PIN6; };
        if (cmdh & 0x8) { PORT_DATA7->BSRR = DATA_PIN7; };
        usec_delay( 5 );
        
        PORT_RE->BSRR = PIN_RE;
        usec_delay( 5 );
        PORT_RE->BRR = PIN_RE;
        usec_delay( 2 );
        
        // младший полубайт
        PORT_DATA4->BRR = DATA_PIN4;
        PORT_DATA5->BRR = DATA_PIN5;
        PORT_DATA6->BRR = DATA_PIN6;
        PORT_DATA7->BRR = DATA_PIN7;
        if (cmdl & 0x1) { PORT_DATA4->BSRR = DATA_PIN4; };
        if (cmdl & 0x2) { PORT_DATA5->BSRR = DATA_PIN5; };
        if (cmdl & 0x4) { PORT_DATA6->BSRR = DATA_PIN6; };
        if (cmdl & 0x8) { PORT_DATA7->BSRR = DATA_PIN7; };
        
    #else
        
        PORT_DATA0->BRR = DATA_PIN0;
        PORT_DATA1->BRR = DATA_PIN1;
        PORT_DATA2->BRR = DATA_PIN2;
        PORT_DATA3->BRR = DATA_PIN3;
        PORT_DATA4->BRR = DATA_PIN4;
        PORT_DATA5->BRR = DATA_PIN5;
        PORT_DATA6->BRR = DATA_PIN6;
        PORT_DATA7->BRR = DATA_PIN7;
        
        if (cmdl & 0x1) { PORT_DATA0->BSRR = DATA_PIN0; };
        if (cmdl & 0x2) { PORT_DATA1->BSRR = DATA_PIN1; };
        if (cmdl & 0x4) { PORT_DATA2->BSRR = DATA_PIN2; };
        if (cmdl & 0x8) { PORT_DATA3->BSRR = DATA_PIN3; };
        if (cmdh & 0x1) { PORT_DATA4->BSRR = DATA_PIN4; };
        if (cmdh & 0x2) { PORT_DATA5->BSRR = DATA_PIN5; };
        if (cmdh & 0x4) { PORT_DATA6->BSRR = DATA_PIN6; };
        if (cmdh & 0x8) { PORT_DATA7->BSRR = DATA_PIN7; };
        
    #endif
    usec_delay( 5 );
    
    PORT_RE->BSRR = PIN_RE;
    usec_delay( 5 );
    PORT_RE->BRR = PIN_RE;
    PORT_RS->BRR = PIN_RS;
    
    usec_delay( 200 );
}


/*! Функция void LCD1602_WriteUserSymbol( uint8_t number, uint8_t *data )
* Записывает в выбраное место пользовательский символ
* \param uint8_t number − номер ячейки для записи
* \param uint8_t *data − массив описывающий символ
*
* Массив определяется следующим образом:
*       00001110    - 0x0E
*       00011111    - 0x1F
*       00010001    - 0x11
*       00010001    - 0x11
*       00011111    - 0x1F
*       00011111    - 0x1F
*       00011111    - 0x1F
*       00000000    - ОБЯЗАТЕЛЬНО должно быть 0x00
*/
void LCD1602_WriteUserSymbol( const uint8_t number, const uint8_t *data )
{
    uint8_t i = 0;
    
    LCD1602_SendCMD( 0x40 + (uint8_t)(number * 8) );
    
    #pragma unroll (4) 
    while( i++ < 8) LCD1602_SendData( *data++ );
}

/*! Функция void LCD1602_PrintCharOfPosition( uint8_t position, char symbol) − печатает в выбраной позиции символ
* \param uint8_t position − позиция - 0..31
* \param char symbol − символ
*/
void LCD1602_PrintCharOfPosition( const uint8_t position, const char symbol)
{
    LCD1602_CursorToPosition( position );
    LCD1602_SendData( (uint8_t)symbol );
}


/*! Функция void LCD1602_AppendChar( char symbol ) − Дописывает символ
* \param char symbol − символ
*/
void LCD1602_AppendChar( const char symbol )
{
    LCD1602_SendData( (uint8_t)symbol );
}


/*! Функция LCD1602_AppendNumber( const uint32_t number ) − число преобразуя его в строку
* \param uint32_t number − целое число
*/
void LCD1602_AppendNumber( const int32_t number )
{
    char string[32] = {0};
    
    sprintf( string, "%d", number );
    LCD1602_AppendString( string );
}


/*! Функция void LCD1602_PrintCharOfPositionXY( uint8_t X, uint8_t Y, char symbol) − печатает в выбраной позиции символ
* \param uint8_t X − номер ячейки в строке - 0..15
* \param uint8_t Y − номер строки 0 или 1
* \param char symbol − символ
* 
* В левом верхнем углу ячейка с координатами (0;0)
*/
void LCD1602_PrintCharOfPositionXY( const uint8_t X, const uint8_t Y, char symbol)
{
    LCD1602_PrintCharOfPosition( ((X & 0x01) + (uint8_t)((Y & 0x0F) * 16)), symbol );
}

/*! Функция void LCD1602_PrintNumberOfPositionXY( uint8_t X, uint8_t Y, char symbol) − печатает в выбраной позиции число переведенное в строку
* \param uint8_t X − номер ячейки в строке - 0..15
* \param uint8_t Y − номер строки 0 или 1
* \param uint32_t number − целое число
* 
* В левом верхнем углу ячейка с координатами (0;0)
*/
void LCD1602_PrintNumberOfPositionXY( const uint8_t X, const uint8_t Y, const int32_t number )
{
    char string[32] = {0};
    
    sprintf( string, "%d", number );
    LCD1602_PrintStringOfPosition( ((X & 0x01) + (uint8_t)((Y & 0x0F) * 16)), string );
}

/*! Функция void LCD1602_PrintNumberOfPosition( const uint8_t position, const uint32_t number ) − печатает в выбраной позиции число переведенное в строку
* \param uint8_t position − позиция - 0..31
* \param uint32_t number − целое число
* 
* В левом верхнем углу ячейка с координатами (0;0)
*/
void LCD1602_PrintNumberOfPosition( const uint8_t position, const int32_t number )
{
    char string[32] = {0};
    
    sprintf( string, "%d", number );
    LCD1602_PrintStringOfPosition( position, string );
}


/*! Функция void LCD1602_CursorToPosition( uint8_t position )  − Устанавливает курсор в выбраную позицию
* \param uint8_t position − позиция - 0..31
*/
void LCD1602_CursorToPosition( uint8_t position ) 
{
    #if (LCD_TYPE == LCD_1602)
        position += ( position < 16 ) ? 0x80 : 0xB0;
    #else
        if      (position >= 60)     position += 0x98;
        else if (position >= 40)     position += 0x6C;
        else if (position >= 20)     position += 0xAC;
        else                        position += 0x80;
    #endif
    
    LCD1602_SendCMD( (uint8_t)position );
}



/*! Функция void LCD1602_CursorToPosition( uint8_t position )  − Устанавливает курсор в выбраную позицию
* \param uint8_t X − номер ячейки в строке - 0..15
* \param uint8_t Y − номер строки 0 или 1
* 
* В левом верхнем углу ячейка с координатами (0;0)
*/
void LCD1602_CursorToPositionXY( const uint8_t X, const uint8_t Y ) 
{
    LCD1602_CursorToPosition( (X & 0x01) + (uint8_t)((Y & 0x0F) * 16) );
}



/*! Функция void LCD1602_PrintStringOfPosition( uint8_t position, char *str )  − Печатает начиная с выбраной позиции строку
* \param uint8_t position − позиция - 0..31
* \param char *str − Указатель на строку оканчивающуюся нулевым символом "\0"
*
* Поддерживает специальные символы \n и \t
* \n - как и должен переносит на новую строку
* \t - вставляет два пробела
*/
void LCD1602_PrintStringOfPosition( const uint8_t position, const char *str )
{
    uint8_t counter = 0;
    uint8_t i = 0;
    
    LCD1602_CursorToPosition( position );
    #pragma unroll (2) 
    while( *str ) 
    {   
        if (*str == 0x0A) // \n
        {
            LCD1602_CursorToPosition( 16 );
            str++;
        }
        if (*str == 0x09) // \t
        {
            for (i = 0; i < (counter % 3); i++) LCD1602_AppendChar( ' ' );
            str++;
        }
        LCD1602_AppendChar( *str++ );
        counter++;
    }
}


/*! Функция void LCD1602_AppendString( char *str ) − Дописывает строку 
* \param char *str − Указатель на строку оканчивающуюся нулевым символом "\0"
*
* \warning НЕПОДДЕРЖИВАЕТ специальные символы \n и \t
*/
void LCD1602_AppendString( const char *str )
{
    #pragma unroll (2) 
    while( *str ) 
    {
        LCD1602_SendData( (uint8_t) *str++ );
    }
}



/*! Функция void LCD1602_PrintStringOfPositionXY( uint8_t X, uint8_t Y, char *str )  − Печатает начиная с выбраной позиции строку
* \param uint8_t X − номер ячейки в строке - 0..15
* \param uint8_t Y − номер строки 0 или 1
* \param char *str − Указатель на строку оканчивающуюся нулевым символом "\0"
*
* Поддерживает специальные символы \n и \t
* \n - как и должен переносит на новую строку
* \t - вставляет два пробела
*/
void LCD1602_PrintStringOfPositionXY( const uint8_t X, const uint8_t Y, const char *str )
{
    LCD1602_PrintStringOfPosition( ((X & 0x01) + (uint8_t)((Y & 0x0F) * 16)), (char *)str );
}



/*! Функция void LCD1602_Init( void )  − Инициализация дисплея
* Должна быть выполнена перед первым использованием дисплея
*/
void LCD1602_Init( void )
{
    
    #if (LCD1602_USE_BITS == true)
        #if (LCD_TYPE == LCD_2004)
            uint8_t initbufferr[] = { 0x02, 0x2A, 0x0F, 0x01, 0x06};
        #else
            uint8_t initbufferr[] = { 0x20, 0x20, 0x28, 0x0F, 0x06, 0x14, 0x80, 0x00 };
        #endif
    #else
        #if (LCD_TYPE == LCD_2004)
            uint8_t initbufferr[] = { 0x3A, 0x0F, 0x01, 0x06};
        #endif
    #endif
        
    uint8_t counter = 0;
    lcd_cursor = CURSOR_HIDE;
    
    msec_delay(40);
    
    while( counter < sizeof(initbufferr) )
    {
        LCD1602_SendCMD( initbufferr[ counter++ ] );
        msec_delay(2);
    }
    
    LCD1602_CursorMode( lcd_cursor );
    LCD1602_ClearScreen();
}



/*! Функция void LCD1602_ClearScreen( void ) − Очищает экран
*/
void LCD1602_ClearScreen( void )
{
    msec_delay(1);
    LCD1602_SendCMD( 0x01 );
    msec_delay(15);
}



/*! Функция void LCD1602_ScreenOnOff( FunctionalState NewState )  − включает и выключает дисплей
* \param FunctionalState NewState − Новое состояние дисплея
*   ENABLE - включает дисплей
*   DISABLE - выключает дисплей
*/
void LCD1602_ScreenOnOff( const FunctionalState NewState )
{
    LCD1602_SendCMD( ( NewState ? 0xC : 0x8 ) | (uint8_t)lcd_cursor );
}



/*! Функция void LCD1602_CursorToHome( void ) − Устанавливает курсор в начальное положение
*/
void LCD1602_CursorToHome( void )
{
    LCD1602_SendCMD( 0x02 );
    msec_delay(2);
}



/*! Функция void LCD1602_CursorMode( CursoreMode cursor )  − Устанавливает текущий режим отображения курсора
* \param CursoreMode cursor − Новое состояние курсора
*   CURSOR_HIDE   - выключает вывод курсора
*   CURSOR_STATIC - отображается статичным подчеркиванием
*   CURSOR_BLINK  - отображается мигающим прямоугольником
*/
void LCD1602_CursorMode( const CursoreMode cursor )
{
    lcd_cursor = cursor;
    LCD1602_SendCMD( 0x0C | (uint8_t)lcd_cursor );
    usec_delay(50);
}
