/*
* This is an independent project of an individual developer. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
// -----------------------------------------------------------------------------------      
#include "memory.h"

// -----------------------------------------------------------------------------------      
static uint8_t M92M01_MemoryReadStatus(void);
static void M92M01_WaitStatus( const uint8_t bits, const uint8_t wait_state);


// -----------------------------------------------------------------------------------      
__forceinline static void M92M01_ChipSelectCmd( const FunctionalState NewState  )
{
    (NewState == ENABLE) ? (MEMORY_CS_PORT->BSRRH = MEMORY_CS_PIN) : (MEMORY_CS_PORT->BSRRL = MEMORY_CS_PIN);
}


__forceinline static uint8_t M92M01_SendDataByte(const uint8_t byte)
{
  while(SPI_I2S_GetFlagStatus(MEMORY_SPI, SPI_I2S_FLAG_TXE) == RESET){ };
  MEMORY_SPI->DR = byte;
  
  while(SPI_I2S_GetFlagStatus(MEMORY_SPI, SPI_I2S_FLAG_RXNE) == RESET){ };
  return (uint8_t)(MEMORY_SPI->DR & 0xFF);
}



static uint8_t M92M01_MemoryReadStatus(void)
{
	uint8_t result;
    
	M92M01_ChipSelectCmd(ENABLE);
	M92M01_SendDataByte(MEMORY_READ_STATUS);
    
	result = M92M01_SendDataByte( 0x00 );
    
	M92M01_ChipSelectCmd(DISABLE);
	M92M01_msec_delay(2);
    
	return result;
}



static void M92M01_WaitStatus( const uint8_t bits, const uint8_t wait_state)
{
	uint8_t byte;
    uint8_t i = 0;
    
	for(;;)
	{
		M92M01_msec_delay(6);
        
		byte = M92M01_MemoryReadStatus();
        
		if( (i++ == 10) || ((byte & bits) == wait_state) ) break;
	}
}




void M92M01_WriteData(uint32_t addr, uint8_t *data, const uint32_t size)
{
	uint32_t i = 0;
	
	M92M01_ChipSelectCmd(ENABLE);
	M92M01_SendDataByte(MEMORY_WRITE_LATCH_ENABLE);
    
	M92M01_ChipSelectCmd(DISABLE);
	M92M01_WaitStatus(STATUS_WRITE_ENABLE_BIT, STATUS_WRITE_ENABLE_BIT);
	
	M92M01_ChipSelectCmd(ENABLE);
	M92M01_SendDataByte(MEMORY_WRITE_DATA);
	M92M01_SendDataByte((addr >> 16) & 0xff);
	M92M01_SendDataByte((addr >> 8) & 0xff);
	M92M01_SendDataByte(addr & 0xff);
	
	while( i < size )
	{
		M92M01_SendDataByte( data[ i++ ] );

		addr++;
        
        // Если данные превысили размер страницы, пишем новую страницу
		if( ((addr % MEMORY_PAGE_SIZE) == 0) && (i < size) )
		{
			M92M01_ChipSelectCmd(DISABLE);
			M92M01_WaitStatus(STATUS_WRITE_IN_PROGRESS_BIT, 0);

			M92M01_ChipSelectCmd(ENABLE);
			M92M01_SendDataByte(MEMORY_WRITE_LATCH_ENABLE);
            
			M92M01_ChipSelectCmd(DISABLE);
			M92M01_WaitStatus(STATUS_WRITE_ENABLE_BIT, STATUS_WRITE_ENABLE_BIT);

			M92M01_ChipSelectCmd(ENABLE);
			M92M01_SendDataByte(MEMORY_WRITE_DATA);
			M92M01_SendDataByte((addr >> 16) & 0xff);
			M92M01_SendDataByte((addr >> 8) & 0xff);
			M92M01_SendDataByte(addr & 0xff);
		}
	}
	M92M01_ChipSelectCmd(DISABLE);
	M92M01_msec_delay(1);

	M92M01_ChipSelectCmd(ENABLE);
	M92M01_SendDataByte(MEMORY_WRITE_LATCH_DISABLE);
    
	M92M01_ChipSelectCmd(DISABLE);
	M92M01_WaitStatus(STATUS_WRITE_ENABLE_BIT, 0);
}



void M92M01_ReadeData(uint32_t addr, uint8_t *data, const uint32_t size)
{
	uint32_t i = 0;
	
	M92M01_ChipSelectCmd(ENABLE);
	M92M01_SendDataByte(MEMORY_READ_DATA);
	M92M01_SendDataByte((addr >> 16) & 0xff);
	M92M01_SendDataByte((addr >> 8) & 0xff);
	M92M01_SendDataByte(addr & 0xff);
	
    // Если данные превысили размер страницы, читаем из новой страницы
	while( i < size )
	{
		data[i++] = M92M01_SendDataByte(0x00);
		addr++;

		if( ((addr % MEMORY_PAGE_SIZE) == 0) && (i < size) )
		{
			M92M01_ChipSelectCmd(DISABLE);
			M92M01_msec_delay(2);
			
			M92M01_ChipSelectCmd(ENABLE);
			M92M01_SendDataByte(MEMORY_READ_DATA);
			M92M01_SendDataByte((addr >> 16) & 0xff);
			M92M01_SendDataByte((addr >> 8) & 0xff);
			M92M01_SendDataByte(addr & 0xff);
		}
	}
	M92M01_ChipSelectCmd(DISABLE);
	M92M01_msec_delay(2);
}



void M92M01_FullChipErase( void )
{
	M92M01_ChipSelectCmd(ENABLE);
    
	M92M01_SendDataByte(MEMORY_CHIP_ERASE);

	M92M01_ChipSelectCmd(DISABLE);
	M92M01_msec_delay(2);
}

void M92M01_Init( void )
{
    GPIO_InitTypeDef GPIO_InitStructure;
    SPI_InitTypeDef SPI_InitStructure;

    GPIO_PinAFConfig(MEMORY_SCLK_PORT, MEMORY_SCLK_PIN_SRC, MEMORY_SPI_AF);
    GPIO_PinAFConfig(MEMORY_MISO_PORT, MEMORY_MISO_PIN_SRC, MEMORY_SPI_AF);
    GPIO_PinAFConfig(MEMORY_MOSI_PORT, MEMORY_MOSI_PIN_SRC, MEMORY_SPI_AF);

    MEMORY_SPI_RCC_CMD( MEMORY_SPI_RCC, ENABLE );
    RCC_AHB1PeriphResetCmd( MEMORY_CS_RCC | MEMORY_MOSI_RCC | MEMORY_MISO_RCC | MEMORY_SCLK_RCC, ENABLE);

    // Общие настройки выводов
    GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed   = GPIO_Speed_25MHz;
    GPIO_InitStructure.GPIO_OType   = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd    = GPIO_PuPd_UP;
    // CLK 
    GPIO_InitStructure.GPIO_Pin     = MEMORY_SCLK_PIN;
    GPIO_Init(MEMORY_SCLK_PORT, &GPIO_InitStructure);
    // MOSI
    GPIO_InitStructure.GPIO_Pin     = MEMORY_MOSI_PIN;
    GPIO_Init(MEMORY_MOSI_PORT, &GPIO_InitStructure);
    // MISO
    GPIO_InitStructure.GPIO_Pin     = MEMORY_MISO_PIN;
    GPIO_InitStructure.GPIO_OType   = GPIO_OType_OD;
//    GPIO_InitStructure.GPIO_PuPd    = GPIO_PuPd_NOPULL;
    GPIO_Init(MEMORY_MISO_PORT, &GPIO_InitStructure);

    // настройки SPI
    SPI_I2S_DeInit(MEMORY_SPI);	
    SPI_InitStructure.SPI_Direction         = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_DataSize          = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL              = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA              = SPI_CPHA_1Edge;
    SPI_InitStructure.SPI_NSS               = SPI_NSS_Soft;
    SPI_InitStructure.SPI_FirstBit          = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;
    SPI_InitStructure.SPI_CRCPolynomial     = 7;
    SPI_InitStructure.SPI_Mode              = SPI_Mode_Master;
    
    SPI_Init(MEMORY_SPI, &SPI_InitStructure);
    
    // Настройки CS
    GPIO_InitStructure.GPIO_Mode    = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType   = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd    = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Pin     = MEMORY_CS_PIN;
    GPIO_Init(MEMORY_CS_PORT, &GPIO_InitStructure);
    
    M92M01_ChipSelectCmd(DISABLE);
    
	SPI_Cmd(MEMORY_SPI, ENABLE);
	SPI_CalculateCRC(MEMORY_SPI, DISABLE);
}
