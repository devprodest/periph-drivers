/*
* This is an independent project of an individual developer. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
/* Copyright (C) gutor.by & gutor.org Lab Team. 2012-2016. All rights reserved. */

#if defined(FREERTOS_CONFIG_H)
    /* Kernel includes. */
    #include "FreeRTOS.h"
    #include "task.h"
#endif

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "proj_config.h"
#include "crc.h"
#include "memory.h"
#include "SettingsManager.h"
#include "logger/logger.h"

extern void _VAR_Lock(void);
extern void _VAR_UnLock(void);

#define	SETTINGS_MANAGER_CAN_WRITE 	1
#define	DEBUG_EACH_NOT_VALID_BLOCK 	0

uint32_t checkValidBlock(save_data_region_t *region, uint32_t i);
void writeBlock(save_data_region_t *region, uint32_t i);
void readBlock(save_data_region_t *region, uint32_t i);

#define REGION_IX_UNKNOWN									0
#define REGION_IX_START										1

typedef struct region_header_struct {
	uint32_t Magic;
	uint32_t IX;
	uint32_t CRC16;
	uint32_t Size;
} region_header_t;

void FindDataFromMemoryRegion(save_data_region_t *region, void *data, int datasize, uint32_t region_start_byte, uint32_t region_stop_byte, uint32_t region_magic)
{
	uint32_t i, max_ix, ix, max_ix_i;
	
	region->DataSize = datasize;
	region->DataPtr = data;
	region->Magic = region_magic;
	
	#if !SETTINGS_MANAGER_CAN_WRITE
	ERROR("Settings manager write protection is ON. Make SETTINGS_MANAGER_CAN_WRITE = 1.");
	#endif
	/* calculating the right block we need  */
	region->BlockSize = (datasize + sizeof(region_header_t)) / SPI_MEMORY_PAGE_SIZE;
	i = datasize % SPI_MEMORY_PAGE_SIZE;
	/* Когда последний блок используем не полностью. */
	if(i > 0) {
		region->BlockSize++;
	}
	region->BlockSize *= SPI_MEMORY_PAGE_SIZE;
	
	/* calculating first used bute in region */
	region->StartAdddr = region_start_byte / SPI_MEMORY_PAGE_SIZE;
	i = region_start_byte % SPI_MEMORY_PAGE_SIZE;
	/* Переходим на начало блока нового, если это не оно. */
	if(i > 0) {
		region->StartAdddr++;
	}
	region->StartAdddr *= SPI_MEMORY_PAGE_SIZE;
	
	/* calculating maxinum avaliable block */
	/* В функции задаётся именно последний используемый байт области используемой (включительно).*/
	/* Если указать байт соответствующий началу страницы, то она не будет использовться. */
	i = region_stop_byte - region->StartAdddr + 1;
	region->LastBlockNumber = i / region->BlockSize;
	region->LastBlockNumber--;
	
	DEBUGMSG("REGION %08x: magic: %08x, size: %d, start byte: %u, stop byte: %u, ", data, region_magic, datasize, region_start_byte, region_stop_byte);
	DEBUGMSG("REGION: BlockSize: %u, StartAdddr: %u, LastBlockNumber: %u", region->BlockSize, region->StartAdddr, region->LastBlockNumber);
	/* finding max_ix */
	max_ix = REGION_IX_UNKNOWN;
	for(i = 0; i <= region->LastBlockNumber; i++) {
		/* Checking all blocks  */
		ix = checkValidBlock(region, i);
		if(ix > max_ix)
		{
			max_ix = ix;
			max_ix_i = i;
		}
	}
	if(max_ix == REGION_IX_UNKNOWN) {
		ERROR("No valid data from all REGION %08x: Magic: %08x, DataSize: %d", data, region_magic, datasize);
		ERROR("    StartAdddr: %u, LastBlockNumber: %u, region_stop_byte: %u", region_start_byte, region->LastBlockNumber, region_stop_byte);
		ClearMemoryRegion(region);
		region->IX = REGION_IX_START;
		writeBlock(region, 0); // region  from 0 to region->LastBlockNumber
		region->i = 0;
		return;
	}
	region->i = max_ix_i;
	readBlock(region, max_ix_i);
	INFO("REGION %08x: Use block %u with IX=%u.", region->DataPtr, region->i, region->IX);
}

void ClearMemoryRegion(save_data_region_t *region)
{
	uint32_t i, addr;
	region_header_t header0;
	
	header0.Size = 0;
	header0.Magic = 0;
	header0.CRC16 = 0;
	header0.IX = 0;
	for(i = 0; i <= region->LastBlockNumber; i++) {
		addr = region->StartAdddr + region->BlockSize * i;
		memory_write_data(addr, (unsigned char *)&header0, sizeof(region_header_t));
	}
	region->i = 0;
	DEBUGMSG("Clear all REGION %08x: Magic: %08x, DataSize: %d, StartAdddr: %u, LastBlockNumber: %u", region->DataPtr, region->Magic, region->DataSize, region->StartAdddr, region->LastBlockNumber);
}


void SaveMemoryRegion(save_data_region_t *region)
{
	if(region->i < region->LastBlockNumber)
	{
		region->i++;
	}
	else
	{
		region->i = 0;
	}
	writeBlock(region, region->i);
}



/**
 * ix = checkValidBlock(region).
 * 
 * Проверяем блок по всем правилам.
 * Если блок правильный, то возвращаем ix.
 * Возвращаем REGION_IX_UNKNOWN == 0 если блок не верный.
 */
uint32_t checkValidBlock(save_data_region_t *region, uint32_t i)
{
	uint32_t addr;
	uint16_t crc;
	region_header_t header0;
	
	addr = region->StartAdddr + region->BlockSize * i;
	memory_read_data(addr, (unsigned char *)&header0, sizeof(region_header_t));

	if(header0.Magic != region->Magic)
	{
		#if DEBUG_EACH_NOT_VALID_BLOCK
		DEBUGMSG("    bl/addr: %u/%u, bad magic - must be: 0x%08x, read: 0x%08x", i, addr, region->Magic, header0.Magic);
		#endif
		return REGION_IX_UNKNOWN;
	}
	if(header0.IX == REGION_IX_UNKNOWN)
	{
		#if DEBUG_EACH_NOT_VALID_BLOCK
		DEBUGMSG("    bl/addr: %u/%u, unknown IX - must be not %u", i, addr, REGION_IX_UNKNOWN);
		#endif
		return REGION_IX_UNKNOWN;
	}
	if(header0.Size != region->DataSize)
	{
		#if DEBUG_EACH_NOT_VALID_BLOCK
		DEBUGMSG("    bl/addr: %u/%u, bad size - must be %u, read %u", i, addr, region->DataSize, header0.Size);
		#endif
		return REGION_IX_UNKNOWN;
	}
	/* checking right crc16 */
	crc = memory_crc16_data(addr + sizeof(region_header_t), region->DataSize);
	if(header0.CRC16 != crc)
	{
		#if DEBUG_EACH_NOT_VALID_BLOCK
		DEBUGMSG("    bl/addr: %u/%u, bad CRC16: must be: 0x%04x, read: 0x%04x", i, addr, header0.CRC16, crc);
		#endif
		return REGION_IX_UNKNOWN;
	}	
	return header0.IX;
}


void writeBlock(save_data_region_t *region, uint32_t i)
{
	#if SETTINGS_MANAGER_CAN_WRITE
	uint32_t addr;
	region_header_t header0;
	
	addr = region->StartAdddr + region->BlockSize * i;
	header0.Size = region->DataSize;
	header0.Magic = region->Magic;
	header0.CRC16 = crc16(region->DataPtr, region->DataSize);
	region->IX++;
	header0.IX = region->IX;
	/* Write data to memory */
	memory_write_data(addr, (unsigned char *)&header0, sizeof(region_header_t));
	_VAR_Lock();
	memory_write_data(addr + sizeof(region_header_t), region->DataPtr, region->DataSize);
	_VAR_UnLock();
	
	if(checkValidBlock(region, i) == REGION_IX_UNKNOWN)
	{
		ERROR("Can't write data to REGION %08x: Magic: %08x, DataSize: %d, StartAdddr: %u, LastBlockNumber: %u", 
			region->DataPtr, region->Magic, region->DataSize, region->StartAdddr, region->LastBlockNumber);
		return;
	}
	INFO("REGION %08x: Write block %u with IX=%u successfully to addr %u", region->DataPtr, region->i, region->IX, addr);
	#endif
}


void readBlock(save_data_region_t *region, uint32_t i)
{
	uint32_t addr;
	uint32_t ix;
	
	ix = checkValidBlock(region, i);
	if(ix != REGION_IX_UNKNOWN)
	{
		addr = region->StartAdddr + region->BlockSize * i;
		_VAR_Lock();
		memory_read_data(addr + sizeof(region_header_t), region->DataPtr, region->DataSize);
		_VAR_UnLock();
		region->IX = ix;
	}
	else
	{
		ERROR("Can't read data to REGION %08x: Magic: %08x, DataSize: %d, StartAdddr: %u, LastBlockNumber: %u", region->DataPtr, region->Magic, region->DataSize, region->StartAdddr, region->LastBlockNumber);
	}
}
