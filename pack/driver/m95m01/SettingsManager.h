/* Copyright (C) gutor.by & gutor.org Lab Team. 2012-2016. All rights reserved. */
#ifndef __SETTINGS_MANAGER_H__
#define __SETTINGS_MANAGER_H__

#include "bsp.h"

typedef struct save_data_region_struct {
	uint32_t Magic;
	uint32_t BlockSize;
	uint32_t StartAdddr;
	uint32_t LastBlockNumber;
	void    *DataPtr;
	uint32_t DataSize;
	uint32_t IX;
	uint32_t i;
} save_data_region_t;


void FindDataFromMemoryRegion(save_data_region_t *region, void *data, int datasize, uint32_t region_start_byte, uint32_t region_stop_byte, uint32_t region_magic);
void SaveMemoryRegion(save_data_region_t *region);
void ClearMemoryRegion(save_data_region_t *region);

#endif /* __SETTINGS_MANAGER_H__ */
