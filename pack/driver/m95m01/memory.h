#ifndef M95M01_H
#define M95M01_H


#ifdef __cplusplus
extern "C" {
#endif


//----- Подключаемые файлы -------------------------------------------------------------------------------
#include <stdint.h>
#ifdef STM32F2XX
    #include "stm32f2xx.h"
#endif
#ifdef STM32F1XX
    #include "stm32f10x.h"
#endif
//--------------------------------------------------------------------------------------------------------
/*< ИСПОЛЬЗОВАНИЕ ФУНКЦИЙ FREERTOS
*   Если определен, то задержка в милисекундах будет считаться силами операционной системы, иначе циклом 
* вызова функции микросекундных зедаржек
*/
#define M92M01_USE_FREERTOS        1               /**< Определяет будет ли использоваться FREERTOS */

#ifdef M92M01_USE_FREERTOS

    #include "FreeRTOS.h"
    #include "task.h"
    
#endif

#define MCU_CLOCK_USECOND   (1 << 4)        /**< Количество тиков счетчика микроконтроллера в микросекунду */



//----- Настройки ----------------------------------------------------------------------------------------
#define MEMORY_SPI              SPI3
#define MEMORY_SPI_AF           GPIO_AF_SPI3
#define MEMORY_SPI_RCC          RCC_APB1Periph_SPI3
#define MEMORY_SPI_RCC_CMD      RCC_APB1PeriphClockCmd


#define MEMORY_CS_PORT          GPIOA
#define MEMORY_CS_PIN           GPIO_Pin_15
#define MEMORY_CS_PIN_SRC       GPIO_PinSource15
#define MEMORY_CS_RCC           RCC_AHB1Periph_GPIOA

#define MEMORY_MOSI_PORT        GPIOC
#define MEMORY_MOSI_PIN         GPIO_Pin_12
#define MEMORY_MOSI_PIN_SRC     GPIO_PinSource12
#define MEMORY_MOSI_RCC         RCC_AHB1Periph_GPIOC

#define MEMORY_MISO_PORT        GPIOC
#define MEMORY_MISO_PIN         GPIO_Pin_11
#define MEMORY_MISO_PIN_SRC     GPIO_PinSource11
#define MEMORY_MISO_RCC         RCC_AHB1Periph_GPIOC

#define MEMORY_SCLK_PORT        GPIOC
#define MEMORY_SCLK_PIN         GPIO_Pin_10
#define MEMORY_SCLK_PIN_SRC     GPIO_PinSource10
#define MEMORY_SCLK_RCC         RCC_AHB1Periph_GPIOC

//----- Константы команд и запросов микросхемы памяти --------------------------------------------------
#define MEMORY_PAGE_SIZE          		    256     /**< Размер страницы*/

#define MEMORY_WRITE_LATCH_ENABLE  		    0x06    /**< Разрешить запись данных */
#define MEMORY_WRITE_LATCH_DISABLE 		    0x04    /**< Запретить запись данных */
#define MEMORY_READ_STATUS         		    0x05    /**< Чтение регистра статуса*/
#define MEMORY_WRITE_STATUS        		    0x01    /**< запись регистра статуса */
#define MEMORY_READ_DATA           		    0x03    /**< Чтение массива данных */
#define MEMORY_WRITE_DATA          		    0x02    /**< Запись массива данных */

#define MEMORY_CHIP_ERASE          		    0xC7    /**< Команда на очистку памяти */

#define STATUS_WRITE_IN_PROGRESS_BIT        0x01    /**<  */
#define STATUS_WRITE_ENABLE_BIT             0x02    /**<  */



//----- Приватные функции --------------------------------------------------------------------------------

/*! Функция void __forceinline usec_delay( uint32_t count ) − Задержка в микросекундах
* \param uint32_t count − время задержки, не должно быть больше UINT32_MAX / MCU_CLOCK_USECOND
*/
__forceinline static void M92M01_usec_delay( uint32_t count )
{
    assert_param( count >= (UINT32_MAX / MCU_CLOCK_USECOND) );
    assert_param( count == 0 );
    
    count *= MCU_CLOCK_USECOND;
    while( count-- ){ };
}



#ifdef M92M01_USE_FREERTOS

    #define M92M01_msec_delay   vTaskDelay
    
#else

/*! Функция void __forceinline msec_delay( uint32_t count ) − Задержка в милисекундах
* \param uint32_t count − время задержки, не должно быть больше UINT32_MAX
*/
    __forceinline static void M92M01_msec_delay( uint32_t count )
    {
        assert_param(count >= UINT32_MAX);
        assert_param( count == 0 );
        
        while( count-- ) M92M01_usec_delay(1000);
    }

#endif // M92M01_USE_FREERTOS



//----- Экспортируемые функции ---------------------------------------------------------------------------
void M92M01_Init( void );   /// инициализация интерфейса для памяти

void M92M01_WriteData(uint32_t addr, uint8_t *data, const uint32_t size);
void M92M01_ReadeData(uint32_t addr, uint8_t *data, const uint32_t size);

void M92M01_FullChipErase( void );






//----- Обработчики ошибок -----------------------------------------------------------------------------

#if (MCU_CLOCK_USECOND == 0)
    #errore "MCU_CLOCK_USECOND must be equal 1..UINT32_MAX"
#endif


#ifdef __cplusplus
}
#endif
//----- КОНЕЦ ФАЙЛА --------------------------------------------------------------------------------------
#endif /* M95M01_H */
