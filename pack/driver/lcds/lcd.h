/**
  *********************************************************************************************************
  *
  *	This file is part of <<PROJECT NAME >>.
  *	
  *	  << PROJECT NAME >> is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  << PROJECT NAME >> is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with << PROJECT NAME >>.  If not, see <http://www.gnu.org/licenses/>.
  *
  * @file file.h
  * @brief < Кратко о назначениии модуля >
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */

#ifndef FILE_H
#define	FILE_H

/**----- Инклуды -----------------------------------------------------------------------------------------*/
#include <stdint.h>              // Нормальные типы данных
#include <stdbool.h>             // тип boolean
#include <math.h>                // библиотека математических функций

/**----- Макросы константы -------------------------------------------------------------------------------*/


/**----- Макрофункции ------------------------------------------------------------------------------------*/
#define CAT(ARGS)                   __CAT(ARGS)
#define __CAT(x, y)                   x ## y

/**----- Типы данных -------------------------------------------------------------------------------------*/




typedef enum
{
    LCD_HD44780,
    LCD_ST7920,
    LCD_WS0010
} LCD_Controller_t;

typedef enum
{
    LCD_8bit_GPIO,
    LCD_4bit_GPIO,
    LCD_Serial_GPIO,
    LCD_Serial_SPI
} LCD_Interface_t;

typedef enum
{
    LCD_WriteOnly,
    LCD_ReadWrite
} LCD_Transfer_t;

typedef enum
{
    LCD_CURSOR_HIDE     = 0x0,                  ///< Курсор скрыт
    LCD_CURSOR_STATIC   = 0x2,                  ///< Курсор в виде подчеркивания
    LCD_CURSOR_BLINK    = 0x1,                  ///< Курсор в виде мигающей ячейки (прямоугольника)
    LCD_CURSOR_BLINK_   = 0x3                   ///< Курсор в виде мигающей полоски
} LCD_Cursore_t;

typedef enum
{
    LCD_ShiftRigth    = 0x04,
    LCD_ShiftLeft     = 0x00
} LCD_CursoreShifr_t;

typedef enum
{
    DISABLE = 0, 
    ENABLE = !DISABLE
} LCD_State;

typedef struct
{
	void ( *LCD1602_WriteUserSymbol )( const uint8_t, const uint8_t*);
	void ( *LCD1602_CursorToHome )( void );
	void ( *LCD1602_CursorToPosition )( const uint8_t);
	void ( *LCD1602_CursorToPositionXY )( const uint8_t, const uint8_t);
	void ( *LCD1602_CursorMode )( const LCD_Cursore_t );
	void ( *LCD1602_ClearScreen )( void );
	void ( *LCD1602_ScreenOnOff )( const LCD_State ); 
	void ( *LCD1602_PrintCharOfPosition )( const uint8_t, const char );
	void ( *LCD1602_PrintStringOfPosition )( const uint8_t, const char * );
	void ( *LCD1602_PrintNumberOfPosition )( const uint8_t, const int32_t );
	void ( *LCD1602_PrintCharOfPositionXY )( const uint8_t, const uint8_t, const char );
	void ( *LCD1602_PrintStringOfPositionXY )( const uint8_t, const uint8_t, const char );
	void ( *LCD1602_PrintNumberOfPositionXY )( const uint8_t, const uint8_t, const int32_t );
	void ( *LCD1602_AppendChar )( const char );
	void ( *LCD1602_AppendString )( const char * );
	void ( *LCD1602_AppendNumber )( const int32_t );
} LCD_Function_t;

typedef struct
{
    uint32_t            LCD_ID;
    LCD_Controller_t    LCD_Controller;
    LCD_Interface_t     LCD_Interface;
    LCD_Transfer_t      LCD_TransferData;
    LCD_Cursore_t       LCD_Cursore;
    LCD_CursoreShifr_t  LCD_CursoreShift;

    uint32_t*           LCD_SPI;
    void              ( *LCD_SPIInit )( void );
    void              ( *LCD_SPIOut )( uint8_t );

    uint32_t            LCD_RS_Pin;
    uint32_t            LCD_RW_Pin;
    uint32_t            LCD_EN_Pin;
    uint32_t*           LCD_RS_Port;
    uint32_t*           LCD_RW_Port;
    uint32_t*           LCD_EN_Port;

    uint32_t          LCD_DATA0_Pin;
    uint32_t          LCD_DATA1_Pin;
    uint32_t          LCD_DATA2_Pin;
    uint32_t          LCD_DATA3_Pin;
    uint32_t          LCD_DATA4_Pin;
    uint32_t          LCD_DATA5_Pin;
    uint32_t          LCD_DATA6_Pin;
    uint32_t          LCD_DATA7_Pin;
    uint32_t*         LCD_DATA0_Port;
    uint32_t*         LCD_DATA1_Port;
    uint32_t*         LCD_DATA2_Port;
    uint32_t*         LCD_DATA3_Port;
    uint32_t*         LCD_DATA4_Port;
    uint32_t*         LCD_DATA5_Port;
    uint32_t*         LCD_DATA6_Port;
    uint32_t*         LCD_DATA7_Port;
    void            ( *LCD_SetPin )( uint32_t*, uint32_t );
    void            ( *LCD_ResetPin )( uint32_t*, uint32_t );
} LCD_t;




/**----- Глобальные переменные ---------------------------------------------------------------------------*/
/**----- Внешние объявления ------------------------------------------------------------------------------*/
/**----- Глобальные прототипы функций --------------------------------------------------------------------*/
void LCD_Init( LCD_t* LCD_struct );

/**----- Проверка макросов -------------------------------------------------------------------------------*/


/**----- КОНЕЦ ФАЙЛА -------------------------------------------------------------------------------------*/
#endif	/* End of FILE_H */
