/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *
  * @file lm75.h
  * @brief Драйвер для работы с датчиком температуры LM75
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */

#ifndef		LM75_H
#define		LM75_H 


#ifdef __cplusplus
extern "C" {
#endif


/**----- Инклуды -----------------------------------------------------------------------------------------*/
#include <stdint.h>              // Нормальные типы данных
#include <stdbool.h>             // тип boolean
#include <math.h>                // библиотека математических функций
#include <lm75_config.h>

/**----- Макросы константы -------------------------------------------------------------------------------*/
/**----- Макрофункции ------------------------------------------------------------------------------------*/

#define CAT(ARGS)                   __CAT(ARGS)
#define __CAT(x, y)                   x ## y


/**----- Типы данных -------------------------------------------------------------------------------------*/
typedef enum
{
    LM75_TEMP = 0x00,       ///< Температура
    LM75_CONF = 0x01,       ///< Конфигурация
    LM75_THYS = 0x02,       ///< Гистерезис
    LM75_TOS  = 0x03        ///< Выключение по превышению температуры
} LM75_Register_t;


typedef struct
{
    bool        shutdown        :1;
    bool        comparator      :1;
    bool        polarity        :1;
    uint8_t     fault           :2;
    const uint8_t free          :3;
} LM75_Config_t;


typedef enum
{
    LM75_DEC,                               ///< В десятичном формате 255 = 25.5
    LM75_RAW                                ///< В RAW формате, AS IS
} LM75_Data_t;                      ///< Тип выводимых данных

typedef struct
{
    I2C_TypeDef*    I2C_Module;             ///< Укаазтель на модуль
    void          (*I2C_Config )( void );   ///< Указатель на функцию инициализации
    LM75_Data_t     DataType;               ///< Тип получаемых данных
    uint8_t         Address;                ///< Адрес датчика
    uint32_t        TimeOut;                ///< Время ожидания
    bool            inited;
} LM75_t;                           ///< Структера с параметрами датчика


/**----- Глобальные переменные ---------------------------------------------------------------------------*/
/**----- Внешние объявления ------------------------------------------------------------------------------*/
/**----- Глобальные прототипы функций --------------------------------------------------------------------*/

uint16_t    LM75_ReadTemperature( LM75_t *LM75_Struct );
void        LM75_Init( LM75_t *LM75_Struct);

/**----- Проверка макросов -------------------------------------------------------------------------------*/
/**----- КОНЕЦ ФАЙЛА -------------------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif	/* End of FILE_H */
