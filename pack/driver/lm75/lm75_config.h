/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *
  * @file lm75_config.h
  * @brief < Кратко о назначениии модуля >
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */
#ifndef        LM_CONFIG_H
#define        LM_CONFIG_H

/**----- Инклуды -----------------------------------------------------------------------------------------*/
#include <stdint.h>              // Нормальные типы данных
#include <stdbool.h>             // тип boolean
#include <math.h>                // библиотека математических функций


/// НЕОБХОДИМО ВЫБРАТЬ ИЛИ ДОПИСАТЬ ИНКЛУДЫ ДЛЯ STD_PERIPH I2C
//#include "stm32f10x_i2c.h"
//#include "stm32f4xx_i2c.h"
//#include "stm32f30x_i2c.h"
//#include "stm32f37x_i2c.h"
//#include "stm32l1xx_i2c.h"



/**----- Макросы константы -------------------------------------------------------------------------------*/


/**----- Макрофункции ------------------------------------------------------------------------------------*/
#define CAT(ARGS)                   __CAT(ARGS)
#define __CAT(x, y)                   x ## y

/**----- Типы данных -------------------------------------------------------------------------------------*/


/**----- Глобальные переменные ---------------------------------------------------------------------------*/
/**----- Внешние объявления ------------------------------------------------------------------------------*/
/**----- Глобальные прототипы функций --------------------------------------------------------------------*/


/**----- Проверка макросов -------------------------------------------------------------------------------*/


/**----- КОНЕЦ ФАЙЛА -------------------------------------------------------------------------------------*/
#endif	/* End of FILE_H */
