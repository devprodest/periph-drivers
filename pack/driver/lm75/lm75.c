/*
* This is an independent project of an individual developer. Dear PVS-Studio, please check it.
* PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
*/
/**
  *********************************************************************************************************
  *
  *	This file is part of Devprodest Lib.
  *	
  *	  Devprodest Lib is free software: you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation, either version 3 of the License, or
  *	  (at your option) any later version.
  *	
  *	  Devprodest Lib is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *	  GNU General Public License for more details.
  *	
  *	  You should have received a copy of the GNU General Public License
  *	  along with Devprodest Lib.  If not, see <http://www.gnu.org/licenses/>.
  *	
  * @file lm75.c
  * @brief Драйвер для работы с датчиком температуры LM75
  * @version 1.0
  * @authors Zaikin Denis (ZD)
  * 
  * @copyright GNU General Public License
  *********************************************************************************************************
  */
/**----- Инклуды -----------------------------------------------------------------------------------------*/
#include "lm75.h"

/**----- Макросы константы -------------------------------------------------------------------------------*/
/**----- Макросы -----------------------------------------------------------------------------------------*/
/**----- Локальные типы данных ---------------------------------------------------------------------------*/
/**----- Локальные переменные ----------------------------------------------------------------------------*/
/**----- Глобальные переменные ---------------------------------------------------------------------------*/
/**----- Прототипы функций -------------------------------------------------------------------------------*/
static uint16_t LM75_Read( LM75_t *LM75_Struct, LM75_Register_t registr);
static int16_t LM75_ConvertRAW(uint16_t raw);
static void LM75_ResetI2C( LM75_t *LM75_Struct );

/**----- Глобальные функции ------------------------------------------------------------------------------*/
/**
@function LM75_ReadTemperature() − Получает у выбранного датчика значение температуры.
@param LM75_t *LM75_Struct − Указатель на описание датчика
@retval uint16_t значение температуры
 */
uint16_t LM75_ReadTemperature( LM75_t *LM75_Struct )
{
	uint16_t value = 0;
	
	if (LM75_Struct->inited)
	{
		switch (LM75_Struct->DataType)
		{
			case LM75_DEC:
				value = LM75_ConvertRAW( LM75_Read( LM75_Struct, LM75_TEMP ) );
				break;

			case LM75_RAW:
				LM75_Read( LM75_Struct, LM75_TEMP );
				break;

			default:
				value = 0;
				break;
		}
	}
	return value;
}


/**----- Локальные функции -------------------------------------------------------------------------------*/
/**
@function LM75_ResetI2C() − Сбрачывает и переинициализирует модуль I2C
@param LM75_t *LM75_Struct − Указатель на описание датчика
 */
static void LM75_ResetI2C( LM75_t *LM75_Struct )
{
	uint32_t Timeout = 0x1FF;

	I2C_SoftwareResetCmd(LM75_Struct->I2C_Module, ENABLE);
	while(--Timeout){};

	I2C_GenerateSTOP(LM75_Struct->I2C_Module, ENABLE);
    (*LM75_Struct->I2C_Config)();
}

/**
@function LM75_ConvertRAW() − Преобразует данные полученые из датчика в числовой формат.
@param uint16_t raw − Данные из датчика
@retval uint16_t значение температуры (0x801B = 27.5 °С)
 */
static int16_t LM75_ConvertRAW(uint16_t raw)
{
	volatile int16_t value = 0;

	if (raw & 0x0080) value = (int16_t)((raw | 0xFFFFFF00) * 10 - (raw >> 15) * 5);
	else              value = (int16_t)((raw & 0x000000FF) * 10 + (raw >> 15) * 5);
	return value;
}

/**
@function LM75_Init() − Инициализирует выбранный датчик.
@param LM75_t *LM75_Struct − Указатель на описание датчика
 */
void LM75_Init( LM75_t *LM75_Struct)
{
	LM75_Struct->inited = true;
    LM75_Struct->TimeOut = (LM75_Struct->TimeOut) ? LM75_Struct->TimeOut : 0x1FFF;
}



/**
@function LM75_Read() − Читает данные регистра датчика
@param LM75_t *LM75_Struct − Указатель на описание датчика
@param LM75_Register_t registr − адрес регистра 
@retval uint16_t значение регистра
 */
static uint16_t LM75_Read( LM75_t *LM75_Struct, LM75_Register_t registr)
{
    uint16_t value = 0;             // Значение температуры
	volatile uint32_t Timeout;      // Время ожидания ошибки

	// ждем, пока шина осовободится
	Timeout = LM75_Struct->TimeOut;
	while(I2C_GetFlagStatus(LM75_Struct->I2C_Module, I2C_FLAG_BUSY)) 
    { 
        if (Timeout-- == 0) 
        { 
            LM75_ResetI2C( LM75_Struct ); 
            return 1; 
        } 
    };

	I2C_AcknowledgeConfig(LM75_Struct->I2C_Module, ENABLE);

	// Генерируем старт
	I2C_GenerateSTART(LM75_Struct->I2C_Module, ENABLE);
	Timeout = LM75_Struct->TimeOut;
	while(!I2C_CheckEvent(LM75_Struct->I2C_Module, I2C_EVENT_MASTER_MODE_SELECT)) 
    { 
        if (Timeout-- == 0) 
        { 
            LM75_ResetI2C( LM75_Struct ); 
            return 1; 
        } 
    };

	// Посылаем адрес датчика
	I2C_Send7bitAddress(LM75_Struct->I2C_Module, LM75_Struct->Address, I2C_Direction_Transmitter);

	Timeout = LM75_Struct->TimeOut;
	while(!I2C_CheckEvent(LM75_Struct->I2C_Module, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) 
    { 
        if (Timeout-- == 0) 
        { 
            LM75_ResetI2C( LM75_Struct ); 
            return 1; 
        } 
    };

	I2C_SendData(LM75_Struct->I2C_Module, registr);

	Timeout = LM75_Struct->TimeOut;
	while(!I2C_CheckEvent(LM75_Struct->I2C_Module, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) 
    { 
        if (Timeout-- == 0) 
        { 
            LM75_ResetI2C( LM75_Struct ); 
            return 1; 
        } 
    };

	I2C_GenerateSTART(LM75_Struct->I2C_Module, ENABLE);
	Timeout = LM75_Struct->TimeOut;
	while(!I2C_CheckEvent(LM75_Struct->I2C_Module, I2C_EVENT_MASTER_MODE_SELECT)) 
    { 
        if (Timeout-- == 0) 
        { 
            LM75_ResetI2C( LM75_Struct ); 
            return 1; 
        } 
    };

	I2C_Send7bitAddress(LM75_Struct->I2C_Module, LM75_Struct->Address, I2C_Direction_Receiver);

	Timeout = LM75_Struct->TimeOut;
	while(!I2C_CheckEvent(LM75_Struct->I2C_Module, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) 
    { 
        if (Timeout-- == 0) 
        { 
            LM75_ResetI2C( LM75_Struct ); 
            return 1; 
        } 
    };

	Timeout = LM75_Struct->TimeOut;
	while( !I2C_CheckEvent(LM75_Struct->I2C_Module, I2C_EVENT_MASTER_BYTE_RECEIVED) ) 
    { 
        if (Timeout-- == 0) 
        { 
            LM75_ResetI2C( LM75_Struct ); 
            return 1; 
        } 
    };
	value = I2C_ReceiveData(LM75_Struct->I2C_Module);

	I2C_AcknowledgeConfig(LM75_Struct->I2C_Module, DISABLE);

	Timeout = LM75_Struct->TimeOut;
	while( !I2C_CheckEvent(LM75_Struct->I2C_Module, I2C_EVENT_MASTER_BYTE_RECEIVED) ) 
    { 
        if (Timeout-- == 0) 
        { 
            LM75_ResetI2C( LM75_Struct ); 
            return 1; 
        } 
    };
	value += I2C_ReceiveData(LM75_Struct->I2C_Module) << 8;

	I2C_GenerateSTOP(LM75_Struct->I2C_Module, ENABLE);
	return value;
}

/**----- КОНЕЦ ФАЙЛА -------------------------------------------------------------------------------------*/
