**Репозиторий с драйверами и полезными библиотеками.**

Включает в себя Драйверы:
+ ДРАЙВЕР ДИСПЛЕЯ 1602 и 2004
+ ДРАЙВЕР SPI ПАМЯТИ M95M01
+ ДРАЙВЕР ДАТЧИКА ТЕМПЕРАТУРЫ STLM75

Включает в себя Бибилиотеки:
+ usr_math - различные математические функции, которые быстрее, а то и в несколько раз быстрее их стандартных аналогов
    - MATH_hypot(X, Y) − Быстрое вычисление гипотенузы. X и Y положительные целые числа
    - MATH_max(X, Y) − Максимальное значение.
    - MATH_min(X, Y) − Минимальное значение.
    - MATH_exp2(X) − Вычисление 2^X
    - MATH_sqrt(x) − Вычисление квадратного корня.
    - MATH_abs(x) − Вычисление модуля числа.
    - MATH_pow(x, y) - Вычисление степени числа
    - MATH_CRC_16_ccitt() − Вычисление контрольной суммы.
    - MATH_CRC_*16_UserTable() − Вычисление CRC16 с пользовательскими настройками
    - MATH_Butterworth5() - фильтр Баттерворта 5-го порядка
    - MATH_HEXStrToInt() - преобразует строку в число
    - MATH_DECStrToInt() - преобразует строку в число
    - MATH_GetMPPTSunVoltage(u, i, du, di) − Вычисление оптимального напряжения для MPPT
+ ringbuffer - реализация кольцевого буфера
+ CException - облегченная библиотека исключений для C
+ version - реализация автоматически изменяемого номера версии в зависимости от времени компиляции

Включает в себя шаблоны:
+ source.c - шаблон файла исходных кодов для языка Си
+ header.h - шаблон заголовочного файла для языка Си
+ COPYING - Текст лицензии GNU GPL v3 для добавления в проект